package fameuz.com.fameuzapp.retrofit;

import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIReference {

    @POST("{lang}/user/login")
    @FormUrlEncoded
    Call<JsonObject> login(@Path("lang") String language,
                           @Field("email") String emailId,
                           @Field("password") String password,
                           @Field("device_token") String device_token,
                           @Field("client") String client);

    @GET("{lang}/categories")
    Call<JsonObject> getAllCategories(@Path("lang") String language,
                                      @Query("mc_id") String keyword);


    @POST("{lang}/user/register")
    @FormUrlEncoded
    Call<JsonObject> register(@Path("lang") String language,
                              @Field("email") String emailId,
                              @Field("password") String password,
                              @Field("confirm_password") String confirm_password,
                              @Field("device_token") String device_token,
                              @Field("reg_cat") String reg_cat,
                              @Field("client") String client);

    @GET("{lang}/nationality-list")
    Call<JsonObject> getNationality(@Path("lang") String language);

    @FormUrlEncoded
    @POST("{lang}/user/fblogin")
    Call<ResponseBody> registerWithFacebook(@Path("lang") String language,
                                            @Field("facebook_id") String facebook_id, @Field("email") String email,
                                            @Field("client") String client);

    @POST("{lang}/user/register-personal-details")
    @FormUrlEncoded
    Call<JsonObject> personal_Detail_hire(@Header("X-API-KEY") String header, @Path("lang") String language, @Field("alt_email") String alt_email, @Field("sub_category") String sub_category, @Field("first_name") String first_name
            , @Field("last_name") String last_name, @Field("company_name") String company_name, @Field("gender") String gender, @Field("dob") String dob, @Field("nationality") String nationality
            , @Field("city") String city, @Field("country") String country, @Field("locale") String locale, @Field("website") String website, @Field("device_token") String device_token,
                                          @Field("client") String client);

    @POST("{lang}/user/upload-profile-image?register=1")
    @Multipart
    Call<ResponseBody> uploadimage(@Header("X-API-KEY") String header, @Path("lang") String language,
                                   @Part MultipartBody.Part imageFile, @Part("crop_x") RequestBody crop_x, @Part("crop_y") RequestBody crop_y
            , @Part("width") RequestBody width, @Part("height") RequestBody height);


    @POST("{lang}/user/add-bio?register=1")
    @FormUrlEncoded
    Call<JsonObject> upload_bio(@Header("X-API-KEY") String header, @Path("lang") String language,
                                @Field("pub_about") String facebook_id);

    @GET("{lang}/get-model-details")
    Call<JsonObject> getmodeldeatils(@Header("X-API-KEY") String header, @Path("lang") String language);

    @POST("{lang}/user/register-model-details")
    Call<JsonObject> registerModelDetail(@Header("X-API-KEY") String header, @Path("lang") String language,
                                         @Body HashMap<String, Integer> body);


    @POST("{lang}/user/register-personal-details")
    Call<ResponseBody> registerPersonalDetail(@Header("X-API-KEY") String header,
                                              @Path("lang") String language,
                                              @Body HashMap<String, String> body);

    @POST("{lang}/user/upload-music?register=1")
    @Multipart
    Call<ResponseBody> uploadmusic(@Header("X-API-KEY") String header,
                                   @Path("lang") String language,
                                   @Part MultipartBody.Part music_file,
                                   @Part MultipartBody.Part music_thumb,
                                   @Part("music_title") RequestBody music_title,
                                   @Part("music_descr") RequestBody music_descr,
                                   @Part("is_shareable") RequestBody is_shareable);

    @Multipart
    @POST("{lang}/user/upload-showreel?register=1")
    Call<ResponseBody> uploadshowreelwithvideo(@Header("X-API-KEY") String header,
                                               @Path("lang") String language,
                                               @Part MultipartBody.Part imageFile,
                                               @Part("sr_title") RequestBody sr_title,
                                               @Part("showreel_youtube") RequestBody showreel_youtube,
                                               @Part("sr_description") RequestBody sr_description,
                                               @Part("is_shearable") RequestBody is_shearable);


    @GET("{lang}/jobs/{jobs_id}")
    Call<ResponseBody> get_job_details(@Header("X-API-KEY") String header, @Path("lang") String language, @Path("jobs_id") String jobs_id);

    @FormUrlEncoded
    @POST("{lang}/post-job")
    Call<ResponseBody> post_job(@Header("X-API-KEY") String header, @Path("lang") String language,
                                @Field("job_title") String job_title, @Field("job_city") String job_city, @Field("job_state") String job_state,
                                @Field("job_country") String job_country, @Field("job_company") String job_company, @Field("job_start") String job_start,
                                @Field("job_end") String job_end, @Field("job_type") String job_type, @Field("job_currency") String job_currency,
                                @Field("job_payment") String job_payment, @Field("job_skill") String job_skill, @Field("job_category") String job_category,
                                @Field("job_descr") String job_descr);

    @FormUrlEncoded
    @POST("{lang}/apply-job")
    Call<ResponseBody> apply_job(@Header("X-API-KEY") String header, @Path("lang") String language,
                                 @Field("job_id") String job_id);


    @GET("{lang}/jobs")
    Call<ResponseBody> get_all_jobs(@Header("X-API-KEY") String header,
                                    @Path("lang") String language,
                                    @Query("query") String query,
                                    @Query("category") String category,
                                    @Query("place") String place,
                                    @Query("jt") String jt,
                                    @Query("search_date") String search_date);


    @GET("{lang}/user/get-suggested-users")
    Call<ResponseBody> get_suggested_user(@Header("X-API-KEY") String header, @Path("lang") String language);

    @GET("{lang}/my-profile")
    Call<ResponseBody> get_lastest_user(@Header("X-API-KEY") String header, @Path("lang") String language);

    @POST("{lang}/user/forgot-password")
    @FormUrlEncoded
    Call<ResponseBody> forgot_pwd(@Path("lang") String language, @Field("email") String email);

    @GET("{lang}/user/resend-verification")
    Call<ResponseBody> resend_verification(@Header("X-API-KEY") String header,
                                           @Path("lang") String language);

    @GET("{lang}/me")
    Call<ResponseBody> getMe(@Header("X-API-KEY") String header,
                             @Path("lang") String language);
}

package fameuz.com.fameuzapp.retrofit;

import android.util.Log;

import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerAPI {
    private static ServerAPI ourInstance = new ServerAPI();
    String API_BASE_URL = "https://dev.fameuz.com/api/v1/";
    private Retrofit mRetrofit;
    private APIReference apiReference;

    public ServerAPI() {
        // sharedPref = new SharedPref(getInstance().)
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.sslSocketFactory(getSSLSocketFactory());
        httpClient.retryOnConnectionFailure(true);
        httpClient.connectTimeout(7000, TimeUnit.SECONDS);
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.addNetworkInterceptor(logging);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request;
                request = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Authorization-Token", "vYCTypxtEAeR2l4tDeEkrZ_XXuMrN9UUZY3lXCBOTrMqCwYGl3FSGLuJRffjzVUi")
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });

        mRetrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        apiReference = mRetrofit.create(APIReference.class);
    }

    private static SSLSocketFactory getSSLSocketFactory() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            return sslSocketFactory;
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static ServerAPI getInstance() {
        return ourInstance;
    }

    public static void hitapi(int getPersonalDetail, RequestBody body, String language, final APIServerResponse apiServerResponse) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://dev.fameuz.com/api/v1/" + language + "/user/login")
                .header("Content-Type", "application/json")
                .header("Authorization-Token", "vYCTypxtEAeR2l4tDeEkrZ_XXuMrN9UUZY3lXCBOTrMqCwYGl3FSGLuJRffjzVUi")
                .post(body)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {

                apiServerResponse.onError(getPersonalDetail, e);
            }

            @Override
            public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.e("url", "myResponse " + myResponse);
                Log.e("url", "myResponse " + response.isSuccessful());
                apiServerResponse.onSuccessString(getPersonalDetail, myResponse);

            }
        });
    }

    public static void postapi(int getPersonalDetail, RequestBody body, String language, String apikey, final APIServerResponse apiServerResponse) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://dev.fameuz.com/api/v1/" + language + "/post-job")
                .header("Content-Type", "application/json")
                .header("Authorization-Token", "vYCTypxtEAeR2l4tDeEkrZ_XXuMrN9UUZY3lXCBOTrMqCwYGl3FSGLuJRffjzVUi")
                .header("X-API-KEY", apikey)
                .post(body)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {

                apiServerResponse.onError(getPersonalDetail, e);
            }

            @Override
            public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.e("url", "myResponse " + myResponse);
                Log.e("url", "myResponse " + response.isSuccessful());
                apiServerResponse.onSuccessString(getPersonalDetail, myResponse);

            }
        });
    }

    public static void facebookapi(int getPersonalDetail, RequestBody body, String language, final APIServerResponse apiServerResponse) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://dev.fameuz.com/api/v1/" + language + "/user/fblogin")
                .header("Content-Type", "application/json")
                .header("Authorization-Token", "vYCTypxtEAeR2l4tDeEkrZ_XXuMrN9UUZY3lXCBOTrMqCwYGl3FSGLuJRffjzVUi")
                .post(body)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                apiServerResponse.onError(getPersonalDetail, e);
            }

            @Override
            public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {
                final String myResponse = response.body().string();
                Log.e("url", "myResponse " + myResponse);
                Log.e("url", "myResponse " + response.isSuccessful());
                apiServerResponse.onSuccessString(getPersonalDetail, myResponse);
            }
        });
    }

    public static void facebookapifinalstep(int getPersonalDetail, RequestBody body, String language, final APIServerResponse apiServerResponse) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://dev.fameuz.com/api/v1/" + language + "/user/fb-register")
                .header("Content-Type", "application/json")
                .header("Authorization-Token", "vYCTypxtEAeR2l4tDeEkrZ_XXuMrN9UUZY3lXCBOTrMqCwYGl3FSGLuJRffjzVUi")
                .post(body)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                apiServerResponse.onError(getPersonalDetail, e);
            }

            @Override
            public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.e("url", "myResponse " + myResponse);
                Log.e("url", "myResponse " + response.isSuccessful());
                apiServerResponse.onSuccessString(getPersonalDetail, myResponse);

            }
        });
    }

    public void login(final int tag, String language, String email, String password, String deviceToken, final APIServerResponse apiServerResponse) {
        RequestBody emailReg = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody passwordReq = RequestBody.create(MediaType.parse("text/plain"), password);
        RequestBody deviceTokenReq = RequestBody.create(MediaType.parse("text/plain"), deviceToken);
        Call<JsonObject> jsonObjectCall = apiReference.login(language, email, password, deviceToken, "android");
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void registerWithFacebook(final int tag, String language, String facebook_id, String email, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.registerWithFacebook(language, facebook_id, email, "android");
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(tag, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void register(final int tag, String language, String email, String password, String confirmPassword, String regCategory, String deviceToken, final APIServerResponse apiServerResponse) {
        Call<JsonObject> jsonObjectCall = apiReference.register(language, email, password, confirmPassword, deviceToken, regCategory, "ANDROID");
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllCategories(final int tag, String language, String mainId, final APIServerResponse apiServerResponse) {
        Call<JsonObject> jsonObjectCall = apiReference.getAllCategories(language, mainId);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getjobdetails(final int tag, String api_key, String language, String mainId, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.get_job_details(api_key, language, mainId);
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful())
                        apiServerResponse.onSuccessString(tag, response.body().string());
                    else
                        apiServerResponse.onSuccessString(tag, response.errorBody().string());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
                @Override
                public void onFailure (Call < ResponseBody > call, Throwable t){
                    apiServerResponse.onError(tag, t);
                }
            });
        }

    public void getNationality(final int tag, String language, final APIServerResponse apiServerResponse) {
        Call<JsonObject> jsonObjectCall = apiReference.getNationality(language);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void personal_detail(final int tag, String apikey, String alternate_email_id, String sub_cat, String first_name, String last_name, String comp_name, String gender_, String dob, String nationality, String city, String counbtry, String locale, String website, String device_token, String client, final APIServerResponse apiServerResponse) {
        Call<JsonObject> jsonObjectCall = apiReference.personal_Detail_hire(apikey, "en", alternate_email_id, sub_cat, first_name, last_name, comp_name, gender_, dob, nationality,
                city, counbtry, locale, website, device_token, client);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.e("onSuccess", "t " + response.body().toString());
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());

                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void uploadimage(int getPersonalDetail, String languageVal, String apikey, File user_dp, final APIServerResponse apiServerResponse) {
        Log.e("user_dp", "user_dp " + apikey);
        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), user_dp);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", user_dp.getName(), reqFile);
        RequestBody crop_x = RequestBody.create(MediaType.parse("multipart/form-data"), "10");
        RequestBody crop_y = RequestBody.create(MediaType.parse("multipart/form-data"), "10");
        RequestBody width = RequestBody.create(MediaType.parse("multipart/form-data"), "300");
        RequestBody height = RequestBody.create(MediaType.parse("multipart/form-data"), "300");

        retrofit2.Call<ResponseBody> req = apiReference.uploadimage(apikey, languageVal, body, crop_x, crop_y, width, height);

        req.enqueue(new Callback<ResponseBody>() {


            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // String  res = null;
                Log.e("onSuccess", getPersonalDetail + " t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.body());


                try {
                    apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });


    }

    public void uploadbio(int getPersonalDetail, String api_key, String apikey, String s, final APIServerResponse apiServerResponse) {
        Call<JsonObject> jsonObjectCall = apiReference.upload_bio(api_key, apikey, s);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.e("onSuccess", "t " + response.body().toString());
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());

                apiServerResponse.onSuccess(getPersonalDetail, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });

    }

    public void showreel(int getPersonalDetail, String languageVal, String apikey, File usershowreel, String showreel_youtube,
                         String sr_title, String sr_description, final APIServerResponse apiServerResponse) {

        Call<ResponseBody> req;
        MultipartBody.Part body = null;
        RequestBody reqFile;
        if (usershowreel != null) {
            reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), usershowreel);
            body = MultipartBody.Part.createFormData("user_showreel", usershowreel.getName(), reqFile);
        }
        RequestBody crop_y = RequestBody.create(MediaType.parse("multipart/form-data"), sr_title);
        RequestBody width = RequestBody.create(MediaType.parse("multipart/form-data"), sr_description);
        RequestBody showreel_youtubereq = RequestBody.create(MediaType.parse("multipart/form-data"), showreel_youtube);
        RequestBody isshear = RequestBody.create(MediaType.parse("multipart/form-data"), "1");
        Log.e("user_dp", "user_dp " + usershowreel);

        req = apiReference.uploadshowreelwithvideo(apikey, languageVal, body, crop_y, showreel_youtubereq, width, isshear);

        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful())
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    else
                        apiServerResponse.onSuccessString(getPersonalDetail, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });

    }

    public void getmodeldetail(int getPersonalDetail, String languageVal, final APIServerResponse apiServerResponse, String apikey) {
        Log.e("json ", "json " + getPersonalDetail);
        Call<JsonObject> jsonObjectCall = apiReference.getmodeldeatils(apikey, languageVal);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.e("onSuccess", "t " + response.body().toString());
                Log.e("onSuccess", getPersonalDetail + "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());
                Log.e("onSuccess", "t " + response);

                apiServerResponse.onSuccess(getPersonalDetail, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });

    }

    public void registerData(int getPersonalDetail, String languageVal, HashMap<String, Integer> list, String apikey, final APIServerResponse apiServerResponse) {
        Call<JsonObject> req = apiReference.registerModelDetail(apikey, languageVal, list);
        req.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());
                apiServerResponse.onSuccess(getPersonalDetail, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("exception", t.getMessage());
                apiServerResponse.onError(getPersonalDetail, t);
            }

        });
    }

    public void registerPersonalData(int getPersonalDetail, String languageVal, String apikey, HashMap<String, String> list,
                                     final APIServerResponse apiServerResponse) {
        Call<ResponseBody> req = apiReference.registerPersonalDetail(apikey, languageVal, list);
        req.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, response.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("exception", t.getMessage());
                apiServerResponse.onError(getPersonalDetail, t);
            }

        });
    }

    public void get_suggested_user(int getPersonalDetail, String apikey, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> req = apiReference.get_suggested_user(apikey, "en");
        req.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                if (response.isSuccessful()) {
                    try {
                        String respon = response.body().string();
                        Log.e("onSuccess", "respon " + respon);

                        apiServerResponse.onSuccessString(getPersonalDetail, respon);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("exception", t.getMessage());
                apiServerResponse.onError(getPersonalDetail, t);
            }

        });
    }

    public void get_latest_user(int getPersonalDetail, String languageVal, String apikey, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> req = apiReference.get_lastest_user(apikey, languageVal);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String respon = response.body().string();
                        apiServerResponse.onSuccessString(getPersonalDetail, respon);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("exception", t.getMessage());
                apiServerResponse.onError(getPersonalDetail, t);
            }

        });
    }

    public void showmusic(int uploadMusic, String languageVal, String api_key, File music_file, File user_dp, String s, String s1, String chec,
                          final APIServerResponse apiServerResponse) {
        MultipartBody.Part musicbody = null, body = null;
        if (music_file != null) {
            RequestBody musicreqFile = RequestBody.create(MediaType.parse("multipart/form-data"), music_file);
            musicbody = MultipartBody.Part.createFormData("music_file", music_file.getName(), musicreqFile);
        }
        if (user_dp != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), user_dp);
            body = MultipartBody.Part.createFormData("music_thumb", user_dp.getName(), reqFile);
        }

        RequestBody music_title = RequestBody.create(MediaType.parse("multipart/form-data"), s);
        RequestBody music_descr = RequestBody.create(MediaType.parse("multipart/form-data"), s1);
        RequestBody is_shareable = RequestBody.create(MediaType.parse("multipart/form-data"), chec);
        retrofit2.Call<ResponseBody> req = apiReference.uploadmusic(api_key, languageVal, musicbody, body, music_title, music_descr,
                is_shareable);

        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // String  res = null;
                Log.e("onSuccess", uploadMusic + " t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.body());
                apiServerResponse.onSuccess(uploadMusic, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiServerResponse.onError(uploadMusic, t);
            }
        });

    }

    public void forgot_pwd(int getPersonalDetail, String languageVal, String email, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.forgot_pwd(languageVal, email);
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {

                    apiServerResponse.onSuccessString(getPersonalDetail, "");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });

    }

    public void applyforjob(int getPersonalDetail, String languageVal, String apikey, String s, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.apply_job(apikey, languageVal, s);
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                Log.e("onSuccess", "t " + response.body().toString());
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, "");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });

    }

    public void getalljobs(int getPersonalDetail, String languageVal, String apikey, String query, String category, String place, String jt, String search_date, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.get_all_jobs(apikey, languageVal, query, category, place, jt, search_date);
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, "");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });

    }

    public void postjobs(int getPersonalDetail, String apikey, String job_tittle, String job_city, String job_state,
                         String job_country, String job_company, String job_start, String job_end,
                         String job_skill, String job_category, String job_descr, String job_type, String job_payment,
                         String job_currency, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.post_job(apikey, "en", job_tittle, job_city, job_state,
                job_country, job_company, job_start, job_end, job_type, job_payment, job_currency, job_skill, job_category
                , job_descr);
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("onSuccess", "t " + new String(response.raw().toString()));
                Log.e("onSuccess", "t " + response.isSuccessful());
                Log.e("onSuccess", "t " + response.message());
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, "");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });

    }

    public void resend_verification(int getPersonalDetail, String languageVal, String apikey, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.resend_verification(apikey, languageVal);
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, "");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });
    }

    public void getMeDetails(int getPersonalDetail, String languageVal, String apikey, final APIServerResponse apiServerResponse) {
        Call<ResponseBody> jsonObjectCall = apiReference.getMe(apikey, languageVal);
        jsonObjectCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        apiServerResponse.onSuccessString(getPersonalDetail, response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    apiServerResponse.onSuccessString(getPersonalDetail, "");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("t", "t " + new String(t.getMessage().toString()));
                apiServerResponse.onError(getPersonalDetail, t);
            }
        });
    }

    public class AddHeaderInterceptor implements Interceptor {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {

            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader("Content-Type", "application/json");
            builder.addHeader("Authorization-Token", "10joYdtzBwhgkI97E60UuoTJSSAFb5g_c8s5jnTesZKPiqmoVp98pfOgukSu8Aip");

            return chain.proceed(builder.build());
        }
    }
}

        package fameuz.com.fameuzapp.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetalljobsPojo {

    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("job_country")
    @Expose
    private String jobCountry;
    @SerializedName("job_state")
    @Expose
    private String jobState;
    @SerializedName("job_city")
    @Expose
    private String jobCity;
    @SerializedName("job_company")
    @Expose
    private String jobCompany;
    @SerializedName("job_start")
    @Expose
    private String jobStart;
    @SerializedName("job_end")
    @Expose
    private String jobEnd;
    @SerializedName("job_skill")
    @Expose
    private String jobSkill;
    @SerializedName("job_category")
    @Expose
    private String jobCategory;
    @SerializedName("job_descr")
    @Expose
    private String jobDescr;
    @SerializedName("job_image")
    @Expose
    private String jobImage;
    @SerializedName("job_image_approved")
    @Expose
    private Integer jobImageApproved;
    @SerializedName("job_currency")
    @Expose
    private String jobCurrency;
    @SerializedName("job_payment")
    @Expose
    private Integer jobPayment;
    @SerializedName("job_created")
    @Expose
    private String jobCreated;
    @SerializedName("job_edited")
    @Expose
    private String jobEdited;
    @SerializedName("job_status")
    @Expose
    private Integer jobStatus;
    @SerializedName("is_approved")
    @Expose
    private Integer isApproved;
    @SerializedName("reject_reason")
    @Expose
    private String rejectReason;
    @SerializedName("user")
    @Expose
    private User user;

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobCountry() {
        return jobCountry;
    }

    public void setJobCountry(String jobCountry) {
        this.jobCountry = jobCountry;
    }

    public String getJobState() {
        return jobState;
    }

    public void setJobState(String jobState) {
        this.jobState = jobState;
    }

    public String getJobCity() {
        return jobCity;
    }

    public void setJobCity(String jobCity) {
        this.jobCity = jobCity;
    }

    public String getJobCompany() {
        return jobCompany;
    }

    public void setJobCompany(String jobCompany) {
        this.jobCompany = jobCompany;
    }

    public String getJobStart() {
        return jobStart;
    }

    public void setJobStart(String jobStart) {
        this.jobStart = jobStart;
    }

    public String getJobEnd() {
        return jobEnd;
    }

    public void setJobEnd(String jobEnd) {
        this.jobEnd = jobEnd;
    }

    public String getJobSkill() {
        return jobSkill;
    }

    public void setJobSkill(String jobSkill) {
        this.jobSkill = jobSkill;
    }

    public String getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(String jobCategory) {
        this.jobCategory = jobCategory;
    }

    public String getJobDescr() {
        return jobDescr;
    }

    public void setJobDescr(String jobDescr) {
        this.jobDescr = jobDescr;
    }

    public String getJobImage() {
        return jobImage;
    }

    public void setJobImage(String jobImage) {
        this.jobImage = jobImage;
    }

    public Integer getJobImageApproved() {
        return jobImageApproved;
    }

    public void setJobImageApproved(Integer jobImageApproved) {
        this.jobImageApproved = jobImageApproved;
    }

    public String getJobCurrency() {
        return jobCurrency;
    }

    public void setJobCurrency(String jobCurrency) {
        this.jobCurrency = jobCurrency;
    }

    public Integer getJobPayment() {
        return jobPayment;
    }

    public void setJobPayment(Integer jobPayment) {
        this.jobPayment = jobPayment;
    }

    public String getJobCreated() {
        return jobCreated;
    }

    public void setJobCreated(String jobCreated) {
        this.jobCreated = jobCreated;
    }

    public String getJobEdited() {
        return jobEdited;
    }

    public void setJobEdited(String jobEdited) {
        this.jobEdited = jobEdited;
    }

    public Integer getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Integer jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Integer getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(Integer isApproved) {
        this.isApproved = isApproved;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



public class User {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
}
package fameuz.com.fameuzapp.retrofit;

import java.util.HashMap;

import retrofit2.Response;

public interface APIServerResponse {
    int LOGIN = 1;
    int REGISTER = 2;
    int FORGOT_PASSWORD = 3;
    int GET_ALL_CATEGORIES = 4;
    int GET_NATIONALITY = 5;
    int SOCIAL_LOGIN = 6;
    int GET_PERSONAL_DETAIL = 7;
    int GET_ABOUT_INFO = 8;
    int UPLOAD_PHOTO = 9;
    int UPLOAD_BIO = 10;
    int UPLOAD_SHOWREEL = 11;
    int GET_DETAILS = 12;
    int UPLOAD_MUSIC = 14;
    int GET_JOB_DETAILS = 15;
    int APPLYFORPOST = 16;
    int SEARCHALLJOBS = 17;
    int GET_SUGGESTED_USER = 18;
    int GET_LATEST_JOB = 19;
    int FORGOT_PWD = 20;
    int POST_JOB = 21;
    int RESEND_VERIFICATION = 22;
    int SOCIAL_LOGINNEW = 23;
    int GET_ME = 24;

    void onSuccess(int tag, Response response);

    void onError(int tag, Throwable throwable);
    void onSuccessString(int tag, String response);
}

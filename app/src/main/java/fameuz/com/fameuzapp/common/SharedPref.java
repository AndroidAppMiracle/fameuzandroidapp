package fameuz.com.fameuzapp.common;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    SharedPreferences sharedPref;
    Context context;
    SharedPreferences.Editor editor;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    public SharedPref(Context context){
        this.context = context;
        sharedPref = context.getSharedPreferences(MY_PREFS_NAME, 0);
    }


    public String getSetC_name() {
        return sharedPref.getString("setC_name", "");
    }

    public void setSetC_name(String setC_name) {

        editor = sharedPref.edit();
        editor.putString("setC_name", setC_name);  // Saving string

        editor.commit();
    }

    String user_id;

    public String getUserid() {
        return sharedPref.getString("user_id", "");
    }

    public void setUserid(String user_id) {

        editor = sharedPref.edit();
        editor.putString("user_id", user_id);  // Saving string

        editor.commit();
    }

    String setC_name;

    public String getIsTalent() {
        return sharedPref.getString("isTalent", "");
    }

    public void setIsTalent(String isTalent) {
        editor = sharedPref.edit();
        editor.putString("isTalent", isTalent);  // Saving string

        editor.commit();
    }

    String isTalent;

    public String getIsModel() {
        return sharedPref.getString("isModel", "");
    }

    public void setIsModel(String isModel) {
        editor = sharedPref.edit();
        editor.putString("isModel", isModel);  // Saving string

        editor.commit();
    }

    public String getIsemail() {
        return sharedPref.getString("isemail", "");
    }

    public void setIsemail(String isemail) {
        this.isemail = isemail;
        editor = sharedPref.edit();
        editor.putString("isemail", isemail);  // Saving string

        editor.commit();
    }

    String isemail;

    public String getApi_key() {
        return sharedPref.getString("api_key", "");
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
        editor = sharedPref.edit();
        editor.putString("api_key", api_key);  // Saving string

        editor.commit();
    }

    String api_key;

    public String getIsAgency() {
        return sharedPref.getString("isAgency", "");
    }

    public void setIsAgency(String isAgency) {
        this.isAgency = isAgency;
        editor = sharedPref.edit();
        editor.putString("isAgency", isAgency);  // Saving string

        editor.commit();
    }

    String isAgency;

    public String getHasMusicCatgeory() {
        return sharedPref.getString("hasMusicCatgeory", "");
    }

    public void setHasMusicCatgeory(String hasMusicCatgeory) {
        this.hasMusicCatgeory = hasMusicCatgeory;
        editor = sharedPref.edit();
        editor.putString("hasMusicCatgeory", hasMusicCatgeory);  // Saving string

        editor.commit();
    }

    String hasMusicCatgeory;

    String user_name;

    public String getUser_name() {
        return sharedPref.getString("user_name", "");
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
        this.hasMusicCatgeory = hasMusicCatgeory;
        editor = sharedPref.edit();
        editor.putString("user_name", user_name);  // Saving string

        editor.commit();
    }

    public String getUser_img() {
        return sharedPref.getString("user_img", "");
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
        editor = sharedPref.edit();
        editor.putString("user_img", user_img);  // Saving string

        editor.commit();
    }

    String user_img;

    public String getIsonetime() {
        return sharedPref.getString("isonetime", "");
    }

    public void setIsonetime(String isonetime) {
        this.isonetime = isonetime;
        editor = sharedPref.edit();
        editor.putString("isonetime", isonetime);  // Saving string

        editor.commit();
    }

    String isonetime;

    public String getIs_user_verified() {
        return sharedPref.getString("is_user_verified", "");
    }

    public void setIs_user_verified(String is_user_verified) {
        this.is_user_verified = is_user_verified;
        editor = sharedPref.edit();
        editor.putString("is_user_verified", is_user_verified);  // Saving string

        editor.commit();
    }

    String is_user_verified;

    public String getIsPPApproved() {
        return sharedPref.getString("is_pp_approved", "");
    }

    public void setIsPPApproved(String is_pp_approved) {
        this.is_pp_approved = is_pp_approved;
        editor = sharedPref.edit();
        editor.putString("is_pp_approved", is_pp_approved);  // Saving string

        editor.commit();
    }

    String is_pp_approved;

    public String getIs_userage() {
        return sharedPref.getString("is_userage", "");
    }

    public void setIs_userage(String is_userage) {
        this.is_userage = is_userage;
        editor = sharedPref.edit();
        editor.putString("is_userage", is_userage);  // Saving string

        editor.commit();
    }

    String is_userage;

    public String getLanguageVal() {
        return sharedPref.getString("val", "");
    }

    public void setLanguageVal(String is_userage) {
        this.is_userage = is_userage;
        editor = sharedPref.edit();
        editor.putString("val", is_userage);  // Saving string

        editor.commit();
    }
}

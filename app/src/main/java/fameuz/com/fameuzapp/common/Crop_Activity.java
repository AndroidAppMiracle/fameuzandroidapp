package fameuz.com.fameuzapp.common;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


import com.theartofdev.edmodo.cropper.CropImageView;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;

public class Crop_Activity extends BaseActivity implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener, View.OnClickListener {

    private CropImageView mCropImageView;
    private ProgressDialog dialog;
    int type;
    private Uri imageUri;
    private Bitmap cropped;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_activity);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            imageUri = Uri.parse(getIntent().getStringExtra("imageUri"));
        }

        //====Initialize Here====//
        init();

        //====Set CropImageView Here====//
        setCropImageView();
        clicks();

    }

    private void clicks() {
        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCropImageView.getCroppedImageAsync();
            }
        });
    }

    void init() {
        dialog = new ProgressDialog(Crop_Activity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Cropping Image...");
        mCropImageView =  findViewById(R.id.CropImageView);
    }

    void setCropImageView() {
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);
        mCropImageView.setFixedAspectRatio(true);
        mCropImageView.setImageUriAsync(imageUri);
        mCropImageView.setShowProgressBar(false);
        mCropImageView.setFixedAspectRatio(true);
        mCropImageView.setAspectRatio(10, 10);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
    }

    @Override
    public void onClick(View view) {

    }

    class CropingImageAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            GlobalConstant.Photo = cropped;
            Intent i = new Intent();
            setResult(RESULT_OK, i);
            finish();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {
            Bitmap bitmap = result.getBitmap();
            if (bitmap != null) {
                cropped = bitmap;
                if (cropped != null) {
                    new CropingImageAsync().execute();
                }
            }
        } else {
            Log.e("AIC", "Failed to crop image", result.getError());
        }
    }
}
package fameuz.com.fameuzapp.utils;

public class Constants {
    public static final String INTERNET_CONNECTION = "Please check your internet connection.";
    public static String FCM_TOKEN = "token";
    public static final String DEVICE_ID = "Device_ID";
    public static final String SHARED_PREF_NAME = "Fameuz";
}


package fameuz.com.fameuzapp.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by satoti.garg on 9/9/2016.
 */
public class ToastMsg {

    public static void showShortToast(Context context, String message) {
        try {
            if (context != null) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showLongToast(Context context, String message) {
        try {
            if (context != null) {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}

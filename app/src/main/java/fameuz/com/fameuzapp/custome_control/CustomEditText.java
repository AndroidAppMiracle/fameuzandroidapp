package fameuz.com.fameuzapp.custome_control;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import fameuz.com.fameuzapp.R;


public class CustomEditText extends android.support.v7.widget.AppCompatEditText {
    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(attrs);

    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);


    }

    public CustomEditText(Context context) {
        super(context);

        init(null);

    }

    private void init(AttributeSet attrs) {
        if (!isInEditMode()) {

            if (attrs != null) {
                TypedArray a = getContext().obtainStyledAttributes(attrs,
                        R.styleable.CustomViews);
                String fontName = a.getString(R.styleable.CustomViews_fontName);
                if(fontName==null){

                    fontName = "Raleway-Bold.ttf";
                }
                if (fontName != null) {
                    Typeface myTypeface = Typeface.createFromAsset(getContext()
                            .getAssets(), "fonts/" + fontName);
                    setTypeface(myTypeface);
                }
                a.recycle();

            }
        }

    }
}


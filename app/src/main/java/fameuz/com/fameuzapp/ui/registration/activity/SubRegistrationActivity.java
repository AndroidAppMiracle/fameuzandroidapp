package fameuz.com.fameuzapp.ui.registration.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.registration.adapters.SubRegisterationAdapter;
import fameuz.com.fameuzapp.common.GlobalConstant;
import fameuz.com.fameuzapp.ui.registration.fragments.AddingBioFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.BasicDetailFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadMusicFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadPhotoFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadShowReelFragment;

public class SubRegistrationActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    public TabLayout tabLayout;
    // private CustomViewPager viewPager;
    SubRegisterationAdapter adapter;
    TextView title_toolbar;
    String intentdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_registration);
        if (getIntent().getExtras() != null) {
            intentdata = getIntent().getExtras().getString("screen");
        }
        init();
        if (GlobalConstant.isuploadphoto) {
            addfragment(new UploadPhotoFragment());
            tabLayout.getTabAt(1).select();
        } else {
            addfragment(new BasicDetailFragment());
        }
    }

    public void init() {
        tabLayout = findViewById(R.id.tabLayout);
        title_toolbar = findViewById(R.id.title_toolbar);

        title_toolbar.setText(getResources().getString(R.string.regis));
        setTitleColor(getResources().getColor(R.color.Black));
        setupTabText();
        tabWidth(tabLayout.getTabCount());
        adapter = new SubRegisterationAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        tabLayout.addOnTabSelectedListener(SubRegistrationActivity.this);
    }

    private void setupTabText() {
        tabLayout.addTab(tabLayout.newTab(), 0);
        tabLayout.addTab(tabLayout.newTab(), 1);
        tabLayout.addTab(tabLayout.newTab(), 2);
        tabLayout.addTab(tabLayout.newTab(), 3);

        TextView tab1 = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tab1.setText(getResources().getString(R.string.personal_detail));
        tab1.setTextSize(11);
        tabLayout.getTabAt(0).setCustomView(tab1);

        TextView tab2 = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tab2.setText(getResources().getString(R.string.upload_photo));
        tab2.setTextSize(11);
        tabLayout.getTabAt(1).setCustomView(tab2);

        TextView tab3 = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tab3.setText(getResources().getString(R.string.boi));
        tab3.setTextSize(11);
        tabLayout.getTabAt(2).setCustomView(tab3);

        TextView tab4 = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tab4.setText(getResources().getString(R.string.upload_sheel));
        tab4.setTextSize(11);
        tabLayout.getTabAt(3).setCustomView(tab4);

        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    public void addTab() {
        tabLayout.addTab(tabLayout.newTab(), 4);
        TextView tab5 = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tab5.setText(getResources().getString(R.string.uploadmusic));
        tab5.setTextSize(11);
        tabLayout.getTabAt(4).setCustomView(tab5);

        replaceFragment(4);
    }

    public void replaceFragment(int position) {
        Fragment fragment = null;
        tabLayout.getTabAt(position).select();

        if (position == 0)
            fragment = new BasicDetailFragment();
        else if (position == 1)
            fragment = new UploadPhotoFragment();
        else if (position == 2)
            fragment = new AddingBioFragment();
        else if (position == 3)
            fragment = new UploadShowReelFragment();
        else if (position == 4)
            fragment = new UploadMusicFragment();


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame, fragment, fragment.getTag());
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public int tabWidth(int count) {
        return getWindowManager().getDefaultDisplay().getWidth() / count;

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        replaceFragment(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //if (viewPager.getCurrentItem() == 0) {

    }

    public void addfragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragment.getTag());
        fragmentTransaction.commit();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
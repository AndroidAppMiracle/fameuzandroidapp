package fameuz.com.fameuzapp.ui.navigationdrawer.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.ui.jobs.PostJobs;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.ui.jobs.SearchJobs;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadMusicFragment;

public class MyProfileFragment extends Fragment {

    TextView txt_username,txt_usercat,txt_userage;
    CircleImageView profileimg,pendingImg;
    View view;
    Button postjob_btn,searchjob_btn;
    SharedPref sharedPref;
    TextView layout_myjobs,layout_mybookings,layout_recieve_appli,layout_my_calender,
            layout_mygallery,layout_ccard,showMusic;

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_profile, container, false);
        sharedPref = new SharedPref(getActivity());
        ids(view);
        settext();
        clicks();
        return view;
    }

    private void clicks() {

        postjob_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPref.getIs_user_verified().equalsIgnoreCase("true")) {
                    Common.addfragment(getActivity(), new PostJobs());
                } else {
                    Toast.makeText(getActivity(), ""+getString(R.string.user_not_verified), Toast.LENGTH_SHORT).show();
                }
            }
        });

        searchjob_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.addfragment(getActivity(),new SearchJobs());
            }
        });

        layout_myjobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.addfragment(getActivity(),new MyJobs());
            }
        });

        layout_mybookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        layout_recieve_appli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        layout_my_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        layout_mygallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        layout_ccard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    private void settext() {
        txt_username.setText(sharedPref.getUser_name());
        if(sharedPref.getIs_userage().equalsIgnoreCase("")){
            txt_usercat.setText(sharedPref.getSetC_name());
        } else {
            txt_usercat.setText(sharedPref.getSetC_name()+",");
            txt_userage.setText(sharedPref.getIs_userage());
        }

        if(sharedPref.getUser_img().length()>0) {
            Glide.with(getActivity())
                    .load(sharedPref.getUser_img())
                    .into(profileimg);

            pendingImg.setVisibility(sharedPref.getIs_user_verified().equalsIgnoreCase("true") ? View.GONE : View.VISIBLE);
            pendingImg.setImageResource(sharedPref.getLanguageVal().equalsIgnoreCase("en")
                    ? R.drawable.pending_for_approval : sharedPref.getLanguageVal().equalsIgnoreCase("pt")
                    ? R.drawable.pending_for_approval_pt : R.drawable.pending_for_approval_ru);
        }
    }

    private void ids(View view) {
        txt_username = view.findViewById(R.id.txt_username);
        txt_usercat = view.findViewById(R.id.txt_usercat);
        txt_userage=view.findViewById(R.id.txt_userage);
        profileimg = view.findViewById(R.id.profileimg);
        postjob_btn = view.findViewById(R.id.postjob_btn);
        searchjob_btn = view.findViewById(R.id.searchjob_btn);
        layout_myjobs =view.findViewById(R.id.layout_myjobs);
        layout_mybookings =view.findViewById(R.id.layout_mybookings);
        layout_recieve_appli =view.findViewById(R.id.layout_recieve_appli);
        layout_my_calender =view.findViewById(R.id.layout_my_calender);
        layout_mygallery =view.findViewById(R.id.layout_mygallery);
        layout_ccard =view.findViewById(R.id.layout_ccard);
        pendingImg =view.findViewById(R.id.pendingImg);
        showMusic =view.findViewById(R.id.showMusic);
    }
    @Override
    public void onResume() {
        super.onResume();
        Common.toolbar_status(getActivity(),true,"");
    }

}

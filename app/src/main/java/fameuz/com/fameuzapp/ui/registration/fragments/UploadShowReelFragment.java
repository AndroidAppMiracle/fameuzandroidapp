package fameuz.com.fameuzapp.ui.registration.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.navigationdrawer.activity.MainActivity;
import fameuz.com.fameuzapp.ui.registration.activity.SubRegistrationActivity;
import fameuz.com.fameuzapp.utils.CommonUtils;
import fameuz.com.fameuzapp.utils.Constants;
import retrofit2.Response;

public class UploadShowReelFragment extends Fragment implements APIServerResponse {

    private static final int SELECT_VIDEO = 100;
    Button continue_btn, upload_later_btn, uploadshowrelbtn;
    SharedPref sharedPref;
    File user_dp;
    String is_sherable = "0";
    TextView showteltitie;
    CheckBox check_allowshowrel, check_accept;
    boolean showtoolbar;
    EditText edit_showreeltit, edit_showreeldesc, showreel_youtube;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload_show_reel, container, false);
        sharedPref = new SharedPref(getActivity());
        if (getArguments() != null) {
            showtoolbar = getArguments().getBoolean("isvalid");
        }
        ids(view);
        clicks();
        return view;
    }

    private void clicks() {

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (user_dp != null) {
                        if (sizeinmb(user_dp, 50)) {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.video_erro), Toast.LENGTH_SHORT).show();
                        } else {
                            hit_continue();
                        }
                    } else {
                        hit_continue();
                    }
                } else {
                    // Toast.makeText(getActivity(), ""+, Toast.LENGTH_SHORT).show();
                }
            }
        });

        upload_later_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertdialog();
            }
        });

        uploadshowrelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean result = CommonUtils.checkPermission(getContext());
                if(result) {
                    Intent intent = new Intent();
                    intent.setType("video/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select a Video"), SELECT_VIDEO);
                }
            }
        });
    }

    private boolean validate() {
        if (edit_showreeltit.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.show_reel_title), Toast.LENGTH_SHORT).show();
            return false;
        } else if (edit_showreeldesc.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.showreel_des), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!check_accept.isChecked()) {
            Toast.makeText(getActivity(), "" + getString(R.string.checkbox_video), Toast.LENGTH_SHORT).show();
            return false;
        } else if (user_dp == null && showreel_youtube.getText().toString().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.selectvideo), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private void ids(View view) {
        continue_btn = view.findViewById(R.id.continue_btn);
        upload_later_btn = view.findViewById(R.id.upload_later_btn);
        showteltitie = view.findViewById(R.id.showteltitie);
        showreel_youtube = view.findViewById(R.id.showreel_youtube);
        check_allowshowrel = view.findViewById(R.id.check_allowshowrel);
        edit_showreeltit = view.findViewById(R.id.edit_showreeltit);
        edit_showreeldesc = view.findViewById(R.id.edit_showreeldesc);
        uploadshowrelbtn = view.findViewById(R.id.uploadshowrelbtn);
        check_accept = view.findViewById(R.id.check_accept);
    }

    public void addfragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragment.getTag());
        fragmentTransaction.commit();
    }

    @Override
    public void onSuccess(int tag, Response response) {


        }

        @Override
        public void onError ( int tag, Throwable throwable){
            ((BaseActivity) getActivity()).hideLoading();
        }

        @Override
        public void onSuccessString ( int tag, String response){
            ((BaseActivity) getActivity()).hideLoading();
            try {
                switch (tag) {
                    case APIServerResponse.UPLOAD_SHOWREEL:
                        JSONObject jsonObject = new JSONObject(response);
                        if(jsonObject.getBoolean("success")) {
                            if (sharedPref.getIsonetime().equalsIgnoreCase("true"))
                                ((BaseActivity) getActivity()).onBackPressed();
                            else {
                                if (sharedPref.getHasMusicCatgeory().equalsIgnoreCase("true")) {
                                    ((SubRegistrationActivity) getActivity()).addTab();
                                } else {
                                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.welcomemsg), Toast.LENGTH_SHORT).show();
                                    getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
                                    getActivity().finish();
                                }
                            }
                        }
                        else {
                            Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            } catch(Exception ex){
                ex.printStackTrace();
            }
        }

    public void hit_continue() {
        //addfragment(new UploadPhotoFragment());
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();

            ServerAPI.getInstance().showreel(APIServerResponse.UPLOAD_SHOWREEL, sharedPref.getLanguageVal(),
                    sharedPref.getApi_key(), user_dp, showreel_youtube.getText().toString(), edit_showreeltit.getText().toString(), edit_showreeldesc.getText().toString(), this);

        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_VIDEO) {
                System.out.println("SELECT_VIDEO");
                Uri selectedImageUri = data.getData();
                String selectedPath = Common.getPath(getActivity(), selectedImageUri);
                Log.e("selectedPath", "selectedPath " + selectedPath);
                if (selectedPath == null || !selectedPath.endsWith(".mp4")) {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.failupload), Toast.LENGTH_SHORT).show();
                } else {
                    user_dp = new File(selectedPath);
                    showteltitie.setText(selectedPath);
                    System.out.println("SELECT_VIDEO Path : " + user_dp.getAbsoluteFile());
                }
            }
        }
    }

    public void alertdialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(getActivity().getResources().getString(R.string.upload_showreel))
                .setMessage(getActivity().getResources().getString(R.string.skip_conf))
                .setPositiveButton(R.string.skip, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        if (sharedPref.getIsonetime().equalsIgnoreCase("true"))
                            ((BaseActivity) getActivity()).onBackPressed();
                        else {
                            if (sharedPref.getHasMusicCatgeory().equalsIgnoreCase("true")) {
                                ((SubRegistrationActivity) getActivity()).addTab();
                            } else {
                                getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
                                getActivity().finish();
                            }
                        }
                    }
                }).show();
    }

    public boolean sizeinmb(File file, int size) {
        long fileSizeInBytes = file.length();
        long fileSizeInKB = fileSizeInBytes / 1024;
        long fileSizeInMB = fileSizeInKB / 1024;

        if (fileSizeInMB > size) {

            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (showtoolbar) {

            Common.toolbar_status(getActivity(), false, "" + getActivity().getString(R.string.upload_showreel));
        }
    }

}

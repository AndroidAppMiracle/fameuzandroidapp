package fameuz.com.fameuzapp.ui.registration.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

import fameuz.com.fameuzapp.Interfaces.OnItemClickListener;
import fameuz.com.fameuzapp.Models.CategoryModel;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.SharedPref;

/**
 * Created by gagandeep.bhutani on 5/21/2018.
 */

public class SubCategoryAdapter  extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> {

    private Context activity;
    ArrayList<CategoryModel> arrayList;
    OnItemClickListener itemClick;

    public void setItemClick(OnItemClickListener itemClick) {
        this.itemClick = itemClick;
    }

    public SubCategoryAdapter(Context activity, ArrayList<CategoryModel> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public SubCategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_cat_adapter, parent, false);
        return new SubCategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubCategoryAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        try{
            holder.sub_Cat.setText(arrayList.get(position).getC_name());

            holder.check.setTag(position);
            holder.check.setVisibility(arrayList.get(position).isStatus()?View.VISIBLE:View.INVISIBLE);

            holder.layout_main.setTag(position);
            holder.layout_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    if (itemClick != null) {
                        itemClick.onItemClick(pos);
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sub_Cat;
        ImageView check;
        LinearLayout layout_main;

        MyViewHolder(View view) {
            super(view);
            sub_Cat=view.findViewById(R.id.sub_Cat);
            check=view.findViewById(R.id.check);
            layout_main=view.findViewById(R.id.layout_main);
        }
    }
}

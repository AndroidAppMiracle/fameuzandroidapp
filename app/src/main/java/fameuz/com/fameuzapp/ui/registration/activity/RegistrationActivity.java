package fameuz.com.fameuzapp.ui.registration.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import fameuz.com.fameuzapp.Models.CategoryModel;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.GlobalConstant;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.GifImageView;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.common.adapter.CategorySubSpinnerAdapter;
import fameuz.com.fameuzapp.ui.login.activity.LoginActivity;
import fameuz.com.fameuzapp.ui.navigationdrawer.activity.MainActivity;
import fameuz.com.fameuzapp.utils.ToastMsg;
import fameuz.com.fameuzapp.utils.Validation;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener, APIServerResponse{

    final String emailPattern = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    Spinner categories_spn, language_spn;
    LinearLayout  layout_hi;
    TextView sign_in_txt;
    TextView txt_hello;
    TextInputEditText email_et_id, pwd_et, cnfirm_pwd;
    JsonObject jsonObject;
    Button register_btn;
    String categoryID = "";
    boolean userIsInteracting;
    Button fb_login_btn;
    SharedPref sharedPref;
    GifImageView gifImageView;
    CategorySubSpinnerAdapter customAdapter;
    CallbackManager callbackManager;
    List<CategoryModel> list = new ArrayList<>();
    String fid = "", femail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        sharedPref = new SharedPref(RegistrationActivity.this);
        init();
        getAllCategories();
        clicks();
        if (getIntent().hasExtra("id")) {
            fid = getIntent().getExtras().getString("id");
            femail = getIntent().getExtras().getString("email");
            email_et_id.setText(femail);
            layout_hi.setVisibility(View.GONE);
            if (!femail.equalsIgnoreCase("")) {
                email_et_id.setEnabled(false);
                email_et_id.setFocusable(false);
            }
        } else {
            email_et_id.setEnabled(true);
            email_et_id.setFocusable(true);
            layout_hi.setVisibility(View.VISIBLE);
        }
        Log.e("fid", "fid " + fid);
    }


    public void init() {
        categories_spn = findViewById(R.id.cat_spn);
        sign_in_txt = findViewById(R.id.sign_in_txt);
        email_et_id = findViewById(R.id.email_et_id);
        pwd_et = findViewById(R.id.pwd_et);
        cnfirm_pwd = findViewById(R.id.confirm_pwd_et);
        register_btn = findViewById(R.id.register_btn);
        language_spn = findViewById(R.id.language_spn);
        layout_hi = findViewById(R.id.layout_hi);
        fb_login_btn = findViewById(R.id.fb_login_btn);
        txt_hello = findViewById(R.id.txt_hello);

        fb_login_btn.setOnClickListener(this);
        sign_in_txt.setOnClickListener(this);
        register_btn.setOnClickListener(this);

        gifImageView = findViewById(R.id.gifImageView);
        gifImageView.setGifAssetPath(GlobalConstant.stringPath);

        changeTextColor(sign_in_txt);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setclickiten();

        categories_spn.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Common.hideSoftKeyboard(RegistrationActivity.this);
                return false;
            }
        });
    }

    public void changeTextColor(TextView textView) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString beforeblackSpannable = new SpannableString(getResources().getString(R.string.already_a_member));
        beforeblackSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, beforeblackSpannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString afterblackSpannable = new SpannableString(" " + getResources().getString(R.string.here));
        afterblackSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, afterblackSpannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        builder.append(beforeblackSpannable);
        builder.append(afterblackSpannable);

        textView.setText(builder);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_txt:
                loginPageCall();
                break;
            case R.id.register_btn:
                register();
                break;
            case R.id.fb_login_btn:
                loginFacebook();
                break;
            default:
                break;
        }
    }

    private void loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(RegistrationActivity.this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                Log.e("error", "error " + object.toString());
                                if (response != null) {

                                    fid = object.optString("id");
                                    String memail = object.optString("email");
                                    registeringFacebookSignup(fid,memail);
                                    if (AccessToken.getCurrentAccessToken() != null) {
                                        LoginManager.getInstance().logOut();
                                    }
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
                Log.e("error", "error " + error.toString());
                Toast.makeText(RegistrationActivity.this, "" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void registeringFacebookSignup(String facebook, String email) {
        if (isConnectedToInternet()) {
            showLoading();
            RequestBody requestBody = new FormBody.Builder()
                    .add("facebook_id", facebook)
                    .add("email", email)
                    .add("client", "android")
                    .build();
            ServerAPI.getInstance().facebookapi(APIServerResponse.SOCIAL_LOGIN, requestBody,  sharedPref.getLanguageVal(),this);
        }
    }

    public void registerFacebookSignup(String email) {
        if (isConnectedToInternet()) {
            showLoading();
            RequestBody requestBody = new FormBody.Builder()
                    .add("facebook_id", fid)
                    .add("email", email)
                    .add("reg_cat", categoryID)
                    .add("client", "android")
                    .build();
            ServerAPI.getInstance().facebookapifinalstep(APIServerResponse.SOCIAL_LOGIN, requestBody, sharedPref.getLanguageVal(),this);
        }
    }

    public void loginPageCall() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    public void register() {
        if (isConnectedToInternet()) {
            if (!fid.equalsIgnoreCase("")) {
                // showLoading();
                if (!categoryID.equalsIgnoreCase("")) {
                    // Toast.makeText(this, ""+email_et_id.getText().toString(), Toast.LENGTH_SHORT).show();
                    if (email_et_id.getText().toString().matches(emailPattern)) {
                        registerFacebookSignup(email_et_id.getText().toString());
                    } else {
                        ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.email_validation));
                        email_et_id.requestFocus();
                    }
                } else {
                    ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.select_group));
                }
            } else {
                if (validateFields(email_et_id.getText().toString(), pwd_et.getText().toString(), cnfirm_pwd.getText().toString())) {
                    showLoading();
                    ServerAPI.getInstance().register(APIServerResponse.REGISTER, sharedPref.getLanguageVal(), email_et_id.getText().toString().trim(),
                            pwd_et.getText().toString().trim(), cnfirm_pwd.getText().toString().trim(),
                            categoryID, getDeviceID(getApplicationContext()), this);
                }
            }
        }
    }

    public void clicks() {
        txt_hello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categories_spn.performClick();
            }
        });
    }

    public void getAllCategories() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllCategories(APIServerResponse.GET_ALL_CATEGORIES, sharedPref.getLanguageVal(), "0", this);
        }
    }

    private boolean validateFields(String username, String password, String confirm_password) {
        if (categoryID.equalsIgnoreCase("")) {
            hideSoftKeyboard(RegistrationActivity.this);
            ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.select_group));
            return false;
        } else if (username.length() <= 0) {
            hideSoftKeyboard(RegistrationActivity.this);
            ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.user_email));
            email_et_id.requestFocus();
            return false;
        } else if (!Validation.validateEmail(username.trim())) {
            hideSoftKeyboard(RegistrationActivity.this);
            ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.email_validation));
            email_et_id.requestFocus();
            return false;
        } else if (password.length() <= 5) {
            hideSoftKeyboard(RegistrationActivity.this);
            ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.password));
            pwd_et.requestFocus();
            return false;
        } else if (confirm_password.length() <= 5) {
            hideSoftKeyboard(RegistrationActivity.this);
            ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.password));
            pwd_et.requestFocus();
            return false;
        } else if (!confirm_password.equals(password)) {
            hideSoftKeyboard(RegistrationActivity.this);
            ToastMsg.showShortToast(RegistrationActivity.this, getResources().getString(R.string.password_mismatch));
            cnfirm_pwd.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.GET_ALL_CATEGORIES:
                        jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("success").getAsBoolean()) {
                            JsonArray jsonArray = jsonObject.get("categories").getAsJsonArray();
                            if (list.size() > 0)
                                list.clear();

                            CategoryModel modelas = new CategoryModel();
                            modelas.setC_name(getResources().getString(R.string.select_category));
                            modelas.setC_id("0");
                            list.add(modelas);
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JsonObject obj = jsonArray.get(i).getAsJsonObject();
                                CategoryModel model = new CategoryModel();
                                model.setC_id(obj.get("c_id").getAsString());
                                model.setC_name(obj.get("c_name").getAsString());
                                list.add(model);
                            }

                            customAdapter = new CategorySubSpinnerAdapter(this, list);

                            categories_spn.setAdapter(customAdapter);
                        } else {
                            ToastMsg.showLongToast(RegistrationActivity.this, jsonObject.get("message").getAsString());
                        }
                        break;
                    case REGISTER:
                        jsonObject = (JsonObject) response.body();
                        Log.e("jsonObject", "json " + jsonObject);
                        if (jsonObject.get("success").getAsBoolean()) {
                            sharedPref.setApi_key(jsonObject.get("result").getAsJsonObject().get("user").getAsJsonObject().get("api_key").getAsString());
                            sharedPref.setUser_name(jsonObject.get("result").getAsJsonObject().get("user").getAsJsonObject().get("display_name").getAsString());
                            Intent intent = new Intent(RegistrationActivity.this, ActivityVerification.class);
                            startActivity(intent);
                            finish();
                        } else {
                            ToastMsg.showLongToast(RegistrationActivity.this, jsonObject.get("message").getAsString());
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                ToastMsg.showLongToast(RegistrationActivity.this, jObjError.getString("message"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void onSuccessString(int tag, String response) {
        hideLoading();
        try {
            JSONObject jsonObjectas = new JSONObject(response);
            switch (tag) {
                case SOCIAL_LOGIN:
                    if (jsonObjectas.optString("message").equalsIgnoreCase(getResources().getString(R.string.select_category))) {
                        Intent intent = new Intent(RegistrationActivity.this, RegistrationActivity.class);
                        intent.putExtra("id", jsonObjectas.optJSONObject("userDetails").optString("facebook_id"));
                        intent.putExtra("email", jsonObjectas.optJSONObject("userDetails").optString("email"));
                        startActivity(intent);
                        finish();
                    } else {
                        if (jsonObjectas.optString("next-step").equalsIgnoreCase("profile-page")) {
                            JSONObject detailUserObjectas = jsonObjectas.getJSONObject("result").getJSONObject("user");
                            if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                                sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                            } else {
                                sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                            }
                            sharedPref.setUserid(detailUserObjectas.optString("user_id"));
                            sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                            sharedPref.setIsemail(detailUserObjectas.optString("email"));
                            if (jsonObjectas.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                                sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                            } else {
                                sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObjectas.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                            }
                            sharedPref.setHasMusicCatgeory(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                            sharedPref.setIsTalent(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                            sharedPref.setIsModel(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                            sharedPref.setIsAgency(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));
                            sharedPref.setIs_user_verified(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                    && jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                    ? "true" : "false");
                            sharedPref.setUser_img(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                    && jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                    ? detailUserObjectas.optString("profile_image")
                                    : detailUserObjectas.optString("pendingProfileImage"));
                            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            runOnUiThread(new Thread(new Runnable() {
                                public void run() {
                                    try {
                                       // Toast.makeText(RegistrationActivity.this, "" + jsonObject.get("message"), 20000).show();
                                        JSONObject detailUserObjectas = jsonObjectas.getJSONObject("result").getJSONObject("user");
                                        if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                                            sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                                        } else {
                                            sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                                        }
                                        sharedPref.setUserid(detailUserObjectas.optString("user_id"));
                                        sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                                        sharedPref.setUser_img(detailUserObjectas.optString("profile_image"));
                                        sharedPref.setIsemail(detailUserObjectas.optString("email"));
                                        if (jsonObjectas.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                                            sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                                        } else {
                                            sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObjectas.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                                        }
                                        sharedPref.setHasMusicCatgeory(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                                        sharedPref.setIsTalent(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                                        sharedPref.setIsModel(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                                        sharedPref.setIsAgency(jsonObjectas.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));
                                        if (jsonObjectas.optString("next-step").equalsIgnoreCase("profile-photo") || jsonObjectas.optString("next-step").equalsIgnoreCase("new-profile-photo")) {
                                            Intent intent = new Intent(RegistrationActivity.this, SubRegistrationActivity.class);
                                            GlobalConstant.isuploadphoto = true;
                                            startActivity(intent);
                                            finish();
                                        } else if (jsonObjectas.optString("next-step").equalsIgnoreCase("activate-account")) {
                                            Intent intent = new Intent(RegistrationActivity.this, ActivityVerification.class);
                                            startActivity(intent);
                                            finish();
                                        } else if (jsonObjectas.optString("next-step").equalsIgnoreCase("register-primary-details") || jsonObjectas.optString("next-step").equalsIgnoreCase("register-model-details"))  {
                                            Intent intent = new Intent(RegistrationActivity.this, SubRegistrationActivity.class);
                                            GlobalConstant.isuploadphoto = false;
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Toast.makeText(RegistrationActivity.this, "" + jsonObject.get("message"), 20000).show();

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }));
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void setclickiten() {
        categories_spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                categoryID = i > 0 ? list.get(i).getC_id() : "";
                Log.e("is first time", userIsInteracting + " id " + i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //  userIsInteracting = true;
    }
}

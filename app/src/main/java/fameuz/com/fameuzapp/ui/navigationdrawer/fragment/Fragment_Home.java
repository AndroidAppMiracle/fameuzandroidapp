package fameuz.com.fameuzapp.ui.navigationdrawer.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import fameuz.com.fameuzapp.Models.SuggestionJobPojo;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.jobs.PostJobs;
import fameuz.com.fameuzapp.ui.jobs.SearchJobs;
import fameuz.com.fameuzapp.ui.navigationdrawer.activity.MainActivity;
import fameuz.com.fameuzapp.ui.navigationdrawer.adapter.CustomPagerAdapter;
import fameuz.com.fameuzapp.ui.registration.fragments.AddingBioFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadMusicFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadPhotoFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadShowReelFragment;
import retrofit2.Response;

public class Fragment_Home extends Fragment implements APIServerResponse {

    View view;
    Button searchjob_btn, postjob_btn, btn_editprof;
    SharedPref sharedPref;
    JSONArray jsonArray;
    RelativeLayout layout_relative;
    ImageView arrowleft, arrowright;
    TextView txt_username, txt_usercat, home_showreel, home_uploadphoto, home_editbio, txt_userage, showMusic,no_jobs;
    ViewPager viewPager;
    int position;
    Bundle bundle;
    CircleImageView profileimg, pendingImg;
    ArrayList<SuggestionJobPojo> arrayList;
    RecyclerView recycler_suggestionuser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        sharedPref = new SharedPref(getActivity());
        getMe();
        return view;
    }

    private void getMe() {
        ServerAPI.getInstance().getMeDetails(APIServerResponse.GET_ME, sharedPref.getLanguageVal(), sharedPref.getApi_key(), this);
    }

    private void settext() {
        txt_username.setText(sharedPref.getUser_name());
        if (sharedPref.getIs_userage().equalsIgnoreCase(""))
            txt_usercat.setText(sharedPref.getSetC_name());
        else {
            txt_usercat.setText(sharedPref.getSetC_name() + ",");
            txt_userage.setText(sharedPref.getIs_userage());
        }
        if (sharedPref.getUser_img().length() > 0) {
            Glide.with(getActivity())
                    .load(sharedPref.getUser_img())
                    .into(profileimg);

            pendingImg.setVisibility(sharedPref.getIs_user_verified().equalsIgnoreCase("true") ? View.INVISIBLE : View.VISIBLE);
            pendingImg.setImageResource(sharedPref.getLanguageVal().equalsIgnoreCase("en")
                    ? R.drawable.pending_for_approval : sharedPref.getLanguageVal().equalsIgnoreCase("pt")
                    ? R.drawable.pending_for_approval_pt : R.drawable.pending_for_approval_ru);
        }

        ((MainActivity)getActivity()).setImage();
    }

    private void clicks() {
        searchjob_btn.setOnClickListener(v -> Common.addfragment(getActivity(), new SearchJobs()));
        postjob_btn.setOnClickListener(v -> postjob());

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int i) {
                //  position = i;
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        arrowright.setOnClickListener(v -> {
            //   Toast.makeText(getActivity(), viewPager.getCurrentItem() + 1+" "+position, Toast.LENGTH_SHORT).show();
            if (viewPager.getCurrentItem() < position)
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        });

        arrowleft.setOnClickListener(v -> {
            //     Toast.makeText(getActivity(), viewPager.getCurrentItem() - 1+" "+position, Toast.LENGTH_SHORT).show();
            if (viewPager.getCurrentItem() < position && viewPager.getCurrentItem() > 0)
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        });

        home_showreel.setOnClickListener(v -> {
            bundle = new Bundle();
            bundle.putBoolean("isvalid", true);
            Common.addfragmentwithbundle(getActivity(), new UploadShowReelFragment(), bundle, true);
        });


        home_uploadphoto.setOnClickListener(v -> {
            bundle = new Bundle();
            bundle.putBoolean("isvalid", true);
            Common.addfragmentwithbundle(getActivity(), new UploadPhotoFragment(), bundle, true);
        });

        home_editbio.setOnClickListener(v -> {
            bundle = new Bundle();
            bundle.putBoolean("isvalid", true);
            Common.addfragmentwithbundle(getActivity(), new AddingBioFragment(), bundle, true);
        });

        showMusic.setOnClickListener(v -> {
            bundle = new Bundle();
            bundle.putBoolean("isvalid", true);
            Common.addfragmentwithbundle(getActivity(), new UploadMusicFragment(), bundle, true);
        });

        btn_editprof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.addfragment(getActivity(), new MyProfileFragment());
            }
        });
    }

    private void postjob() {
        if (sharedPref.getIs_user_verified().equalsIgnoreCase("true")) {
            Common.addfragment(getActivity(), new PostJobs());
        } else {
            Toast.makeText(getActivity(), "" + getString(R.string.user_not_verified), Toast.LENGTH_SHORT).show();
        }
    }

    private void ids(View view) {
        btn_editprof = view.findViewById(R.id.btn_editprof);
        txt_userage = view.findViewById(R.id.txt_userage);
        arrowleft = view.findViewById(R.id.arrowleft);
        arrowright = view.findViewById(R.id.arrowright);
        searchjob_btn = view.findViewById(R.id.searchjob_btn);
        postjob_btn = view.findViewById(R.id.postjob_btn);
        txt_username = view.findViewById(R.id.txt_username);
        no_jobs = view.findViewById(R.id.no_jobs);
        txt_usercat = view.findViewById(R.id.txt_usercat);
        profileimg = view.findViewById(R.id.profileimg);
        home_showreel = view.findViewById(R.id.home_showreel);
        showMusic = view.findViewById(R.id.showMusic);
        home_editbio = view.findViewById(R.id.home_editbio);
        layout_relative = view.findViewById(R.id.layout_relative);
        home_uploadphoto = view.findViewById(R.id.home_uploadphoto);
        pendingImg = view.findViewById(R.id.pendingImg);
        recycler_suggestionuser = view.findViewById(R.id.recycler_suggestionuser);
        viewPager = view.findViewById(R.id.viewpager);
        recycler_suggestionuser.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));

        showMusic.setVisibility(sharedPref.getHasMusicCatgeory().equalsIgnoreCase("true") ? View.VISIBLE : View.GONE);
        settext();
        clicks();
        Common.showprogress(getActivity());
        getlatestjob();
    }

    public void getlatestjob() {
        ServerAPI.getInstance().get_latest_user(APIServerResponse.GET_LATEST_JOB, sharedPref.getLanguageVal(),
                sharedPref.getApi_key(), this);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        Common.dismissprogress();
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        Common.dismissprogress();
    }

    @Override
    public void onSuccessString(int tag, String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.optBoolean("success")) {
                switch (tag) {
                    case APIServerResponse.GET_SUGGESTED_USER:
                        break;

                    case APIServerResponse.GET_LATEST_JOB:
                        Common.dismissprogress();
                        arrayList = new ArrayList<>();
                        jsonArray = jsonObject.optJSONArray("latestJobs");
                        position = jsonArray!=null&&jsonArray.length()>0?jsonArray.length():0;
                        if(position>0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                SuggestionJobPojo suggestionJobPojo = new SuggestionJobPojo();
                                suggestionJobPojo.setIsApproved(jsonArray.optJSONObject(i).optString("is_approved"));
                                suggestionJobPojo.setJobId(jsonArray.optJSONObject(i).optString("job_id"));
                                suggestionJobPojo.setUserId(jsonArray.optJSONObject(i).optString("user_id"));
                                suggestionJobPojo.setJobTitle(jsonArray.optJSONObject(i).optString("job_title"));
                                suggestionJobPojo.setJobSlug(jsonArray.optJSONObject(i).optString("job_slug"));
                                suggestionJobPojo.setJobCountry(jsonArray.optJSONObject(i).optString("job_country"));
                                suggestionJobPojo.setJobState(jsonArray.optJSONObject(i).optString("job_state"));
                                suggestionJobPojo.setJobCity(jsonArray.optJSONObject(i).optString("job_city"));
                                suggestionJobPojo.setJobStart(jsonArray.optJSONObject(i).optString("job_start"));
                                suggestionJobPojo.setJobEnd(jsonArray.optJSONObject(i).optString("job_end"));
                                suggestionJobPojo.setJobSkill(jsonArray.optJSONObject(i).optString("job_skill"));
                                suggestionJobPojo.setJobCategory(jsonArray.optJSONObject(i).optString("job_category"));
                                suggestionJobPojo.setJobDescr(jsonArray.optJSONObject(i).optString("job_descr"));
                                suggestionJobPojo.setJobImage(jsonArray.optJSONObject(i).optString("job_image"));
                                suggestionJobPojo.setJobPayment(jsonArray.optJSONObject(i).optString("job_payment"));
                                suggestionJobPojo.setJobCompany(jsonArray.optJSONObject(i).optString("job_company"));
                                suggestionJobPojo.setJobCreated(jsonArray.optJSONObject(i).optString("job_created"));
                                suggestionJobPojo.setShareLink(jsonArray.optJSONObject(i).optString("shareLink"));
                                arrayList.add(suggestionJobPojo);
                            }
                        }
                        if (arrayList.size() > 0) {
                            setadapter(arrayList);
                            layout_relative.setVisibility(View.VISIBLE);
                            no_jobs.setVisibility(View.GONE);
                        } else {
                            layout_relative.setVisibility(View.GONE);
                            no_jobs.setVisibility(View.VISIBLE);
                        }
                        break;

                    case APIServerResponse.GET_ME:
                        if (jsonObject.optBoolean("success")) {
                            if (jsonObject.optString("next-step").equalsIgnoreCase("profile-page")) {
                                JSONObject detailUserObjectas = jsonObject.getJSONObject("result").getJSONObject("user");
                                if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                                    sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                                } else {
                                    sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                                }
                                sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                                sharedPref.setIsemail(detailUserObjectas.optString("email"));
                                if (jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                                    sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                                } else {
                                    sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                                }
                                sharedPref.setHasMusicCatgeory(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                                sharedPref.setIsTalent(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                                sharedPref.setIsModel(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                                sharedPref.setIsAgency(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));
                                sharedPref.setIs_user_verified(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                        && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                        ? "true" : "false");
                                sharedPref.setUser_img(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                        && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                        ? detailUserObjectas.optString("profile_image")
                                        : detailUserObjectas.optString("pendingProfileImage"));
                                sharedPref.setIs_userage(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("age"));

                                ids(view);
                            }
                        }
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setadapter(ArrayList<SuggestionJobPojo> arrayList) {
        CustomPagerAdapter adapter_sugestedJob = new CustomPagerAdapter(getActivity(), arrayList, getActivity().getSupportFragmentManager());
        viewPager.setAdapter(adapter_sugestedJob);
    }

    @Override
    public void onResume() {
        super.onResume();
        Common.toolbar_status(getActivity(), true, "");
    }
}

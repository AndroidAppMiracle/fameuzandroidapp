package fameuz.com.fameuzapp.ui.navigationdrawer.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.ui.navigationdrawer.adapter.Adapter_FragmentTalentDire;

public class Fragment_Talent_Directory extends Fragment {

    View view;
    EditText edit_search;
    TextView talent_direc_shortlist,talent_direc_invite;
    RecyclerView recycler_talent_dire;
    Adapter_FragmentTalentDire adapter_fragmentTalentDire;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_talent_directory,container,false);
        ids(view);
        return view;
    }

    private void ids(View view) {
        recycler_talent_dire = view.findViewById(R.id.recycler_talent_dire);
        edit_search = view.findViewById(R.id.edit_search);
        talent_direc_shortlist = view.findViewById(R.id.talent_direc_shortlist);
        talent_direc_invite = view.findViewById(R.id.talent_direc_invite);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_talent_dire.setLayoutManager(mLayoutManager);
        recycler_talent_dire.setItemAnimator(new DefaultItemAnimator());
        adapter_fragmentTalentDire = new Adapter_FragmentTalentDire(getActivity());
        recycler_talent_dire.setAdapter(adapter_fragmentTalentDire);
    }
}

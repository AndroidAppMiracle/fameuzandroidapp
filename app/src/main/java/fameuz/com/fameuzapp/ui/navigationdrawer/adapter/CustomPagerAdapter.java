package fameuz.com.fameuzapp.ui.navigationdrawer.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fameuz.com.fameuzapp.Models.SuggestionJobPojo;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.ui.jobs.JobDetails;


public class CustomPagerAdapter  extends PagerAdapter{
    Context context;
    ArrayList<SuggestionJobPojo> images;
    LayoutInflater layoutInflater;
    FragmentManager supportFragmentManager;
    SharedPref sharedPref;

    public CustomPagerAdapter(Context context, ArrayList<SuggestionJobPojo> images, FragmentManager supportFragmentManager) {
        this.context = context;
        this.images = images;
        this.supportFragmentManager=supportFragmentManager;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.e("images","images "+images.size());
        sharedPref = new SharedPref(context);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.adapter_sugestedjob, container, false);
        TextView job_location = itemView.findViewById(R.id.job_location);
        TextView job_category = itemView.findViewById(R.id.job_category);
        TextView job_date = itemView.findViewById(R.id.job_date);
        TextView job_company = itemView.findViewById(R.id.job_company);
        TextView job_payment = itemView.findViewById(R.id.job_payment);
        TextView job_skills = itemView.findViewById(R.id.job_skills);
        TextView job_postedate = itemView.findViewById(R.id.job_postedate);
        TextView job_title = itemView.findViewById(R.id.job_title);
        Button btn_view = itemView.findViewById(R.id.btn_view);
        TextView layout_share = itemView.findViewById(R.id.layout_share);

        job_location.setText(images.get(position).getJobCity()+", "+images.get(position).getJobState()+", "+images.get(position).getJobCountry());

        job_category.setText(images.get(position).getJobCategory());

        job_date.setText(images.get(position).getJobStart());
        job_title.setText(images.get(position).getJobTitle());
        job_company.setText(images.get(position).getJobCompany());

        job_payment.setText(images.get(position).getJobPayment());
        job_skills.setText(images.get(position).getJobSkill());
        job_postedate.setText(images.get(position).getJobCreated());

        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addfragment(new JobDetails(),images.get(position).getJobId());
            }
        });

        layout_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPref.getIs_user_verified().equalsIgnoreCase("true"))
                    Common.shareintent(context,images.get(position).getShareLink());
                else
                    Toast.makeText(context, "" + context.getString(R.string.user_not_verified), Toast.LENGTH_SHORT).show();

            }
        });

        container.addView(itemView);
        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public void addfragment(Fragment fragment, String id){
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragment.getTag());
        fragmentTransaction.addToBackStack(fragment.getTag());
        Bundle bundle = new Bundle();
        bundle.putString("id",id);
        bundle.putBoolean("add",true);
        fragment.setArguments(bundle);
        fragmentTransaction.commit();
    }
}
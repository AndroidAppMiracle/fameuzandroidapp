package fameuz.com.fameuzapp.ui.jobs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fameuz.com.fameuzapp.Models.RelatedJobsPojo;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.ui.common.RelatedJobSerial;
import fameuz.com.fameuzapp.ui.registration.adapters.Adapter_RelatedJob;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;

public class RelatedJobs extends Fragment {

    View view;
    RecyclerView jobs_rv;
    Adapter_RelatedJob adapter_searchJob;
    SharedPref sharedPref;
    TextView txt_header;
    Button btn_postjob;
    ArrayList<RelatedJobsPojo> arrayList;
    RelatedJobSerial relatedJobsPojo;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_searchjobs,container,false);
        if(getArguments()!=null){
            relatedJobsPojo = (RelatedJobSerial) getArguments().getSerializable("related_job");
            arrayList=relatedJobsPojo.getArrayList();
        }
        sharedPref = new SharedPref(getActivity());
        ids(view);
        if(arrayList.size()==0){
            Toast.makeText(getActivity(), ""+getString(R.string.nojobsfound), Toast.LENGTH_SHORT).show();
        }
        setadapter(arrayList);
        return view;
    }

    private void ids(View view) {
        jobs_rv = view.findViewById(R.id.jobs_rv);
        txt_header = view.findViewById(R.id.txt_header);
        btn_postjob = view.findViewById(R.id.btn_postjob);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        jobs_rv.setLayoutManager(mLayoutManager);
        jobs_rv.setItemAnimator(new DefaultItemAnimator());
        txt_header.setText(getActivity().getString(R.string.related_jobs));
        btn_postjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.addfragment(getActivity(),new PostJobs());
            }
        });

    }


    private void setadapter(ArrayList<RelatedJobsPojo> arrayList) {
        adapter_searchJob = new Adapter_RelatedJob(getActivity(),arrayList,getActivity().getSupportFragmentManager());
        jobs_rv.setAdapter(adapter_searchJob);
    }



    @Override
    public void onResume() {
        super.onResume();

            Common.toolbar_status(getActivity(), false, "" + getActivity().getString(R.string.related_jobs));

    }
}


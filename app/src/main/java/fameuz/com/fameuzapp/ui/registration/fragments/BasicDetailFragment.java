package fameuz.com.fameuzapp.ui.registration.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import fameuz.com.fameuzapp.Interfaces.OnItemClickListener;
import fameuz.com.fameuzapp.Models.CategoryModel;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.common.adapter.ChestSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.CollarSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.EyesSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.HairSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.HeightSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.HipsSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.JacketSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.ModeldressSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.NationalitySpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.ShoesSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.TrouserSpinnerAdapter;
import fameuz.com.fameuzapp.ui.common.adapter.WaistSpinnerAdapter;
import fameuz.com.fameuzapp.ui.login.adapter.SpinnerAdapter;
import fameuz.com.fameuzapp.ui.registration.activity.SubRegistrationActivity;
import fameuz.com.fameuzapp.ui.registration.adapters.SubCategoryAdapter;
import fameuz.com.fameuzapp.utils.Constants;
import fameuz.com.fameuzapp.utils.LocaleManager;
import fameuz.com.fameuzapp.utils.ToastMsg;
import retrofit2.Response;

public class BasicDetailFragment extends Fragment implements APIServerResponse, View.OnClickListener, AdapterView.OnItemClickListener, OnItemClickListener, AdapterView.OnItemSelectedListener {
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyBZAt1jK7bI7Q7_0vKS1r1rV61KoFamxsc";
    JsonObject jsonObject;
    Button btn_regicont, continueBtn;
    CheckBox checkterms;
    SharedPref sharedPref;
    RelativeLayout layout_rela;
    LinearLayout layout_personalinfo, layout_companydeatils, personal_specification, main_burst, main_chest, layout_male,
            layout_female, nationality_ll;
    TextView category_name_spn, edit_dob, edit_email, txt_subcat, done;
    EditText edit_companyname, email_et, edit_altemail, alternate_email_id, web_url, nationality, edit_firstname, edit_lastname;
    Spinner spinner_eyes, spinner_height, spinner_waist, spinner_chest, spinner_shoes, nationality_spn, spinner_burst,
            spinner_jacket, spinner_trouser, spinner_hair, spinner_gender, spinner_hips, spinner_collar, spinner_dress;
    AutoCompleteTextView edit_city;
    RecyclerView category_name_spntwo;
    List<String> gender;
    String categoryID = "", categoryName = "", gen_list = "", websitepattern = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&amp;=]*)?";
    JsonObject mainjsonarr;
    CheckBox confirmaage;
    ArrayList<JsonObject> arrhas;
    HashMap<String, String> arrhashing;
    int dates = 0, years = 0, mnths = 0, i = 0, shoe_id = 0, chest_id = 0, collar_id = 0, dress_id = 0, eyes_id = 0, hair_id = 0, height_id = 0, hips_id = 0, jacket_id = 0, trousers_id = 0, waist_id = 0;
    NationalitySpinnerAdapter nationalitySpinnerAdapter;
    ShoesSpinnerAdapter shoesSpinnerAdapter;
    HeightSpinnerAdapter heightSpinnerAdapter;
    ModeldressSpinnerAdapter modeldressSpinnerAdapter;
    ChestSpinnerAdapter chestSpinnerAdapter;
    CollarSpinnerAdapter collarSpinnerAdapter;
    EyesSpinnerAdapter eyesSpinnerAdapter;
    HairSpinnerAdapter hairSpinnerAdapter;
    Spinner language_spn;
    HipsSpinnerAdapter hipsSpinnerAdapter;
    JacketSpinnerAdapter jacketSpinnerAdapter;
    TrouserSpinnerAdapter trouserSpinnerAdapter;
    WaistSpinnerAdapter waistSpinnerAdapter;
    Button closeBtn;
    ArrayList<CategoryModel> list = new ArrayList<>();
    ArrayList<String> ids_list = new ArrayList<>();
    ArrayList<String> names_list = new ArrayList<>();
    Dialog dialog, dialog1;
    String[] langArray = {" English", " Português", " Pусский"};
    Integer[] imageArray = {R.drawable.english, R.drawable.protugese, R.drawable.russia};
    String lang;
    boolean ifmodel, ismodeldetailfilled;
    SubCategoryAdapter adapter;

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("LOG_TAG", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("LOG_TAG", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getJSONArray("terms").getJSONObject(0).getString("value") + "," + predsJsonArray.getJSONObject(i).getJSONArray("terms").getJSONObject(predsJsonArray.getJSONObject(i).getJSONArray("terms").length() - 1).getString("value"));
            }
        } catch (JSONException e) {
            Log.e("LOG_TAG", "Cannot process JSON results", e);
        }
        return resultList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basic_detail, container, false);
        ((BaseActivity) getActivity()).getmPrefs();
        arrhashing = new HashMap<>();
        sharedPref = new SharedPref(getActivity());
        init(view);
        arrhas = new ArrayList<>();
        getCategories();
        return view;
    }

    public void init(View view) {
        edit_lastname = view.findViewById(R.id.edit_lastname);
        edit_email = view.findViewById(R.id.edit_email);
        category_name_spn = view.findViewById(R.id.category_name_spn);
        btn_regicont = view.findViewById(R.id.btn_regicont);
        edit_companyname = view.findViewById(R.id.edit_companyname);
        email_et = view.findViewById(R.id.email_et);
        alternate_email_id = view.findViewById(R.id.alternate_email_id);
        edit_altemail = view.findViewById(R.id.edit_altemail);
        web_url = view.findViewById(R.id.web_url);
        language_spn = view.findViewById(R.id.language_spn);
        checkterms = view.findViewById(R.id.checkterms);
        confirmaage = view.findViewById(R.id.confirmaage);
        edit_city = view.findViewById(R.id.edit_city);
        edit_dob = view.findViewById(R.id.edit_dob);
        txt_subcat = view.findViewById(R.id.txt_subcat);
        layout_rela = view.findViewById(R.id.layout_rela);
        nationality = view.findViewById(R.id.nationality);
        layout_personalinfo = view.findViewById(R.id.layout_personalinfo);
        edit_firstname = view.findViewById(R.id.edit_firstname);
        layout_companydeatils = view.findViewById(R.id.layout_companydeatils);
        spinner_gender = view.findViewById(R.id.spinner_gender);
        nationality_spn = view.findViewById(R.id.nationality_spn);
        nationality_ll = view.findViewById(R.id.nationality_ll);

        if (sharedPref.getIsTalent().equalsIgnoreCase("true")) {
            layout_personalinfo.setVisibility(View.VISIBLE);
            layout_companydeatils.setVisibility(View.GONE);
            nationality_ll.setVisibility(View.VISIBLE);
        } else {
            layout_personalinfo.setVisibility(View.GONE);
            layout_companydeatils.setVisibility(View.VISIBLE);
            nationality_ll.setVisibility(View.GONE);
        }
        //-----SPINNER--------------------------------------------
        category_name_spn.setText(sharedPref.getSetC_name());
        edit_email.setText(sharedPref.getIsemail());
        email_et.setText(sharedPref.getIsemail());
        btn_regicont.setOnClickListener(this);
        layout_rela.setOnClickListener(this);
        language_spn.setOnItemSelectedListener(this);
        spinnerada();
        setSpinnerData();
        edit_city.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
        edit_city.setOnItemClickListener(this);

    }

    public void setSpinnerData() {
        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), R.layout.spinner_sel_lang, langArray, imageArray);
        language_spn.setAdapter(adapter);
        language_spn.setSelection(((BaseActivity) getActivity()).getLanguageValue().length() > 0 ? adapter.getPosition(((BaseActivity) getActivity()).getLanguageValue()) : 0);
        ((BaseActivity) getActivity()).setLanguageValue(language_spn.getSelectedItem().toString());
    }

    public void getCategories() {
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            Log.e("is talent ", "talent " + sharedPref.getApi_key());
            if (sharedPref.getIsTalent().equalsIgnoreCase("true") || sharedPref.getIsModel().equalsIgnoreCase("true")) {
                ServerAPI.getInstance().getAllCategories(APIServerResponse.GET_ALL_CATEGORIES, sharedPref.getLanguageVal(), "2", this);
            } else {
                ServerAPI.getInstance().getAllCategories(APIServerResponse.GET_ALL_CATEGORIES, sharedPref.getLanguageVal(), "1", this);
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void getNationality() {
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getNationality(APIServerResponse.GET_NATIONALITY, sharedPref.getLanguageVal(), this);
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void getaboutinfo() {
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getmodeldetail(APIServerResponse.GET_ABOUT_INFO, sharedPref.getLanguageVal(), this, sharedPref.getApi_key());
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        ((BaseActivity) getActivity()).hideLoading();
        try {
            switch (tag) {
                case APIServerResponse.GET_ALL_CATEGORIES:
                    jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("success").getAsBoolean()) {
                        if (jsonObject.get("success").getAsBoolean()) {
                            categoriespineer(jsonObject.get("categories").getAsJsonArray());

                            getNationality();
                        } else {
                            ToastMsg.showLongToast(getContext(), jsonObject.get("message").getAsString());
                        }
                    }
                    break;

                case APIServerResponse.GET_NATIONALITY:
                    jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("success").getAsBoolean()) {
                        nationalityspineer(jsonObject.get("nationalities").getAsJsonArray());
                        getaboutinfo();
                    } else {
                        ToastMsg.showLongToast(getContext(), jsonObject.get("message").getAsString());
                    }
                    break;

                case APIServerResponse.GET_ABOUT_INFO:
                    jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("success").getAsBoolean()) {
                        getJsonobj(jsonObject.getAsJsonObject("data"));
                    } else {
                        ToastMsg.showLongToast(getContext(), jsonObject.get("message").getAsString());
                    }

                    break;

                case APIServerResponse.GET_DETAILS:
                    jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("success").getAsBoolean()) {
                        if (dialog != null) {
                            resetdata();
                            dialog.dismiss();
                            ismodeldetailfilled = true;
                        }
                    } else {
                        ToastMsg.showLongToast(getContext(), jsonObject.get("message").getAsString());
                    }
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getJsonobj(JsonObject data) {
        mainjsonarr = data;
    }

    private void jsonopendia(JsonObject jsonObject) {
        setShoe(jsonObject.get("model_shoes").getAsJsonArray());
        setEye(jsonObject.get("model_eyes").getAsJsonArray());
        setHeight(jsonObject.get("model_height").getAsJsonArray());
        setDress(jsonObject.get("model_dress").getAsJsonArray());
        setchest(jsonObject.get("model_chest").getAsJsonArray());
        setCollar(jsonObject.get("model_collar").getAsJsonArray());
        setHair(jsonObject.get("model_hair").getAsJsonArray());
        setHip(jsonObject.get("model_hips").getAsJsonArray());
        setJacket(jsonObject.get("model_jacket").getAsJsonArray());
        setTrouser(jsonObject.get("model_trousers").getAsJsonArray());
        setWaist(jsonObject.get("model_waist").getAsJsonArray());
    }

    private void categoriespineer(JsonArray categories) {
        if (list.size() > 0)
            list.clear();

        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.select_category));
        modelas.setC_id("0");
        list.add(modelas);
        for (int i = 0; i < categories.size(); i++) {
            JsonObject obj = categories.get(i).getAsJsonObject();
            if (!obj.get("c_name").getAsString().equalsIgnoreCase(sharedPref.getSetC_name())) {
                CategoryModel model = new CategoryModel();
                model.setC_id(obj.get("c_id").getAsString());
                model.setC_name(obj.get("c_name").getAsString());
                model.setStatus(false);
                list.add(model);
            }
        }
    }

    private void nationalityspineer(JsonArray categories) {
        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.sel_nationality));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < categories.size(); i++) {
            JsonObject obj = categories.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("n_id").getAsString());
            model.setC_name(obj.get("n_name").getAsString());
            newlist.add(model);

        }
        nationalitySpinnerAdapter = new NationalitySpinnerAdapter(getActivity(), newlist);
        nationality_spn.setAdapter(nationalitySpinnerAdapter);

        nationality_spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0)
                    edit_city.requestFocus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        nationality_spn.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Common.hideSoftKeyboard(getActivity());
                return false;
            }
        });
    }

    private void setchest(JsonArray asJsonArray) {
        Log.e("chest", "chest " + asJsonArray.size());
        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        if (gen_list.equalsIgnoreCase("1")) {
            modelas.setC_name(getResources().getString(R.string.chest));
        } else {
            modelas.setC_name(getResources().getString(R.string.burst));
        }
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mc_id").getAsString());
            model.setC_name(obj.get("mc_name").getAsString());
            newlist.add(model);

        }
        Log.e("chest", "newlist " + newlist.size());


        chestSpinnerAdapter = new ChestSpinnerAdapter(getActivity(), newlist);
        spinner_chest.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //    JsonObject detailCategories = newlist.get(i).getAsJsonObject();
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    chest_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spinner_burst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    chest_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setCollar(JsonArray asJsonArray) {
        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.select_collar));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mcollar_id").getAsString());
            model.setC_name(obj.get("mcollar_name").getAsString());
            newlist.add(model);

        }

        collarSpinnerAdapter = new CollarSpinnerAdapter(getActivity(), newlist);
        spinner_collar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //  JsonObject detailCategories = asJsonArray.get(i).getAsJsonObject();
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    collar_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setEye(JsonArray asJsonArray) {

        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.select_eye));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("me_id").getAsString());
            model.setC_name(obj.get("me_name").getAsString());
            newlist.add(model);

        }

        eyesSpinnerAdapter = new EyesSpinnerAdapter(getActivity(), newlist);
        spinner_eyes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    eyes_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setDress(JsonArray asJsonArray) {

        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.dress));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mdress_id").getAsString());
            model.setC_name(obj.get("mdress_name").getAsString());
            newlist.add(model);

        }

        modeldressSpinnerAdapter = new ModeldressSpinnerAdapter(getActivity(), newlist);
        spinner_dress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    dress_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setHair(JsonArray asJsonArray) {


        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.hair));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mhair_id").getAsString());
            model.setC_name(obj.get("mhair_name").getAsString());
            newlist.add(model);

        }

        hairSpinnerAdapter = new HairSpinnerAdapter(getActivity(), newlist);
        spinner_hair.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               /* JsonObject detailCategories = asJsonArray.get(i).getAsJsonObject();
                hair_id = detailCategories.get("mhair_id").getAsInt();*/
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    hair_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setHip(JsonArray asJsonArray) {

        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.hips));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mhp_id").getAsString());
            model.setC_name(obj.get("mhp_name").getAsString());
            newlist.add(model);

        }

        hipsSpinnerAdapter = new HipsSpinnerAdapter(getActivity(), newlist);
        spinner_hips.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
             /*   JsonObject detailCategories = asJsonArray.get(i).getAsJsonObject();
                hips_id = detailCategories.get("mhp_id").getAsInt();*/
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    hips_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setJacket(JsonArray asJsonArray) {


        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.jacket));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mj_id").getAsString());
            model.setC_name(obj.get("mj_name").getAsString());
            newlist.add(model);

        }

        jacketSpinnerAdapter = new JacketSpinnerAdapter(getActivity(), newlist);
        spinner_jacket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                /*JsonObject detailCategories = asJsonArray.get(i).getAsJsonObject();
                jacket_id = detailCategories.get("mj_id").getAsInt();*/
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    jacket_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setTrouser(JsonArray asJsonArray) {


        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.trouser));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mt_id").getAsString());
            model.setC_name(obj.get("mt_name").getAsString());
            newlist.add(model);

        }


        trouserSpinnerAdapter = new TrouserSpinnerAdapter(getActivity(), newlist);
        spinner_trouser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                /*JsonObject detailCategories = asJsonArray.get(i).getAsJsonObject();
                trousers_id = detailCategories.get("mt_id").getAsInt();*/

                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    trousers_id = Integer.parseInt(newlist.get(i).getC_id());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setWaist(JsonArray asJsonArray) {

        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.waist));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mw_id").getAsString());
            model.setC_name(obj.get("mw_name").getAsString());
            newlist.add(model);

        }


        waistSpinnerAdapter = new WaistSpinnerAdapter(getActivity(), newlist);
        spinner_waist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    waist_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setHeight(JsonArray asJsonArray) {


        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.height));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("mh_id").getAsString());
            model.setC_name(obj.get("mh_name").getAsString());
            newlist.add(model);

        }


        heightSpinnerAdapter = new HeightSpinnerAdapter(getActivity(), newlist);
        spinner_height.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                /*JsonObject detailCategories = asJsonArray.get(i).getAsJsonObject();
                height_id = detailCategories.get("mh_id").getAsInt();*/
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    height_id = Integer.parseInt(newlist.get(i).getC_id());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setShoe(JsonArray asJsonArray) {


        List<CategoryModel> newlist = new ArrayList<>();
        if (newlist.size() > 0)
            newlist.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getResources().getString(R.string.shoes));
        modelas.setC_id("0");
        newlist.add(modelas);
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject obj = asJsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("ms_id").getAsString());
            model.setC_name(obj.get("ms_name").getAsString());
            newlist.add(model);
        }

        shoesSpinnerAdapter = new ShoesSpinnerAdapter(getActivity(), newlist);
        spinner_shoes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!newlist.get(i).getC_id().equalsIgnoreCase("0")) {
                    shoe_id = Integer.parseInt(newlist.get(i).getC_id());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void onSuccessString(int tag, String response) {
        ((BaseActivity) getActivity()).hideLoading();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            ((BaseActivity) getActivity()).hideLoading();
            switch (tag) {
                case APIServerResponse.GET_PERSONAL_DETAIL:
                    if (jsonObject.getString("success").equalsIgnoreCase("true")) {
                        sharedPref.setHasMusicCatgeory(jsonObject.optString("hasMusicCategory"));
                        ((SubRegistrationActivity) getActivity()).replaceFragment(1);
                    } else {
                        ToastMsg.showLongToast(getContext(), jsonObject.getString("message"));
                    }
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_regicont:
                Common.hideSoftKeyboard(getActivity());
                ifmodel = sharedPref.getIsModel().equalsIgnoreCase("true") || txt_subcat.getText().toString().toLowerCase().contains("model")
                        ? true : false;

                if (sharedPref.getIsTalent().equalsIgnoreCase("true")) {
                    if (modalvalida()) {
                        if (ifmodel) {
                            if (ismodeldetailfilled)
                                savePersonalInfo(edit_email.getText().toString(), true);
                            else
                                Toast.makeText(getActivity(), "" + getResources().getString(R.string.fillmodel), Toast.LENGTH_SHORT).show();
                        } else {
                            savePersonalInfo(edit_email.getText().toString(), true);
                        }
                    }
                } else if (companyvalida()) {
                    savePersonalInfo(email_et.getText().toString(), false);
                }
                break;

            case R.id.layout_rela:
                Common.hideSoftKeyboard(getActivity());
                datepicker();
                break;

            case R.id.continueBtn:
                if (gender_validation(gen_list)) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                    saveGender();
                }

                break;

            case R.id.done:
                if (dialog1 != null && dialog1.isShowing())
                    dialog1.dismiss();
                txt_subcat.setText(ids_list.size() > 0 ? getIds(ids_list) : getResources().getString(R.string.select_sub_cat1));
                break;
        }
    }

    private boolean gender_validation(String gen_list) {
        if (gen_list.equalsIgnoreCase("1")) {
            if (eyes_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.eyes_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (hair_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.hair_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (height_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.height_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (waist_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.waist_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (hips_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.hips_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (chest_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.chest_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (shoe_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.shoes_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (jacket_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.jacket_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (trousers_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.trouser_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (collar_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.collar_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else {
                return true;
            }
        } else {
            if (eyes_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.eyes_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (hair_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.hair_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (height_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.height_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (waist_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.waist_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (hips_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.hips_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (chest_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.burst_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (shoe_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.shoes_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else if (dress_id == 0) {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.dress_validation), Toast.LENGTH_SHORT).show();
                return false;
            } else {
                return true;
            }
        }


    }

    private void savePersonalInfo(String email, boolean b) {
        sharedPref.setSetC_name(category_name_spn.getText().toString() + ", " + txt_subcat.getText().toString());
        HashMap<String, String> data = new HashMap<>();
        if (b) {
            data.put("email", email);
            data.put("alt_email", alternate_email_id.getText().toString().trim());
            data.put("sub_category", categoryID.toString());
            data.put("first_name", edit_firstname.getText().toString().trim());
            data.put("last_name", edit_lastname.getText().toString().trim());
            data.put("company_name", edit_companyname.getText().toString().trim());
            data.put("gender", gen_list);
            data.put("dob", edit_dob.getText().toString().trim());
            data.put("nationality", nationality.getText().toString().trim());
            data.put("city", edit_city.getText().toString().trim());
            data.put("country", edit_city.getText().toString().trim());
            data.put("locale", sharedPref.getLanguageVal());
            data.put("website", web_url.getText().toString().trim());
            data.put("device_token", "gadkhkashdkshakdsf");
            data.put("client", "android");
        } else {
            data.put("email", email);
            data.put("alt_email", alternate_email_id.getText().toString().trim());
            data.put("sub_category", categoryID.toString());
            data.put("company_name", edit_companyname.getText().toString().trim());
            data.put("nationality", nationality.getText().toString().trim());
            data.put("city", edit_city.getText().toString().trim());
            data.put("country", edit_city.getText().toString().trim());
            data.put("locale", sharedPref.getLanguageVal());
            data.put("website", web_url.getText().toString().trim());
            data.put("device_token", "gadkhkashdkshakdsf");
            data.put("client", "android");
        }
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().registerPersonalData(APIServerResponse.GET_PERSONAL_DETAIL, sharedPref.getLanguageVal(), sharedPref.getApi_key(), data, this);
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    private void saveGender() {
        HashMap<String, Integer> data = new HashMap<>();
        data.put("model_eyes", eyes_id);
        data.put("model_hair", hair_id);
        data.put("model_height", height_id);
        data.put("model_waist", waist_id);
        data.put("model_chest", chest_id);
        data.put("model_shoe", shoe_id);
        data.put("model_dress", dress_id);
        data.put("model_jacket", jacket_id);
        data.put("model_trousers", trousers_id);
        data.put("model_collar", collar_id);
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().registerData(APIServerResponse.GET_DETAILS, sharedPref.getLanguageVal(), data, sharedPref.getApi_key(), this);
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    private void datepicker() {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar selectedCal = Calendar.getInstance();
                selectedCal.set(year, monthOfYear, dayOfMonth);

                long selectedMilli = selectedCal.getTimeInMillis();
                Date datePickerDate = new Date(selectedMilli);
                if (datePickerDate.after(new Date()) || !onsDateSet(year, monthOfYear, dayOfMonth)) {
                    Toast.makeText(getActivity(), "Please select valid date", Toast.LENGTH_SHORT).show();
                } else {
                    years = year;
                    mnths = monthOfYear;
                    dates = dayOfMonth;
                    String date = String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1)
                            + "-" + String.valueOf(dayOfMonth);
                    edit_dob.setText(date);
                }
            }
        }, yy, mm, dd);
        if (years != 0) {
            datePicker.updateDate(years, mnths, dates);
        }
        datePicker.show();
    }

    public void spinnerada() {
        gender = new ArrayList<String>();
        gender.add(getResources().getString(R.string.select_gender));
        gender.add(getResources().getString(R.string.male));
        gender.add(getResources().getString(R.string.female));
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, gender);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_gender.setAdapter(genderAdapter);
        gendercheck();
    }

    private void gendercheck() {
        spinner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Common.hideSoftKeyboard(getActivity());
                if (position > 0) {
                    gen_list = gender.get(position).equalsIgnoreCase(getResources().getString(R.string.male)) ? "1" : "2";
                    if (sharedPref.getIsTalent().equalsIgnoreCase("true")) {
                        dial(gender.get(position));
                        ifmodel = true;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinner_gender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Common.hideSoftKeyboard(getActivity());
                return false;
            }
        });

        txt_subcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1 = spinner(getActivity(), list, this);
            }
        });
    }

    private void gendervisi(String type) {
        if (type.equalsIgnoreCase(getResources().getString(R.string.male))) {
            layout_male.setVisibility(View.VISIBLE);
            layout_female.setVisibility(View.GONE);
            main_chest.setVisibility(View.VISIBLE);
            main_burst.setVisibility(View.GONE);
        } else {
            layout_male.setVisibility(View.GONE);
            layout_female.setVisibility(View.VISIBLE);
            main_chest.setVisibility(View.GONE);
            main_burst.setVisibility(View.VISIBLE);
        }
    }

    public void dial(String s) {
        dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.dialogue_personal_spec);
        personal_specification = dialog.findViewById(R.id.personal_specification);
        layout_male = dialog.findViewById(R.id.layout_male);
        layout_female = dialog.findViewById(R.id.layout_female);
        main_chest = dialog.findViewById(R.id.main_chest);
        main_burst = dialog.findViewById(R.id.main_burst);
        spinner_eyes = dialog.findViewById(R.id.spinner_eyes);
        spinner_height = dialog.findViewById(R.id.spinner_height);
        spinner_waist = dialog.findViewById(R.id.spinner_waist);
        spinner_chest = dialog.findViewById(R.id.spinner_chest);
        spinner_shoes = dialog.findViewById(R.id.spinner_shoes);
        spinner_jacket = dialog.findViewById(R.id.spinner_jacket);
        spinner_trouser = dialog.findViewById(R.id.spinner_trouser);
        spinner_hair = dialog.findViewById(R.id.spinner_hair);
        spinner_hips = dialog.findViewById(R.id.spinner_hips);
        spinner_collar = dialog.findViewById(R.id.spinner_collar);
        spinner_dress = dialog.findViewById(R.id.spinner_dress);
        spinner_burst = dialog.findViewById(R.id.spinner_burst);
        continueBtn = dialog.findViewById(R.id.continueBtn);
        closeBtn = dialog.findViewById(R.id.closeBtn);
        gendervisi(s);
        jsonopendia(mainjsonarr);
        setada();
        dialog.show();
        continueBtn.setOnClickListener(this);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetdata();
                dialog.dismiss();
            }
        });
    }

    public void setada() {
        spinner_shoes.setAdapter(shoesSpinnerAdapter);
        spinner_height.setAdapter(heightSpinnerAdapter);
        spinner_chest.setAdapter(chestSpinnerAdapter);
        spinner_burst.setAdapter(chestSpinnerAdapter);
        spinner_eyes.setAdapter(eyesSpinnerAdapter);
        spinner_hair.setAdapter(hairSpinnerAdapter);
        spinner_jacket.setAdapter(jacketSpinnerAdapter);
        spinner_trouser.setAdapter(trouserSpinnerAdapter);
        spinner_waist.setAdapter(waistSpinnerAdapter);
        spinner_hips.setAdapter(hipsSpinnerAdapter);
        spinner_collar.setAdapter(collarSpinnerAdapter);
        spinner_dress.setAdapter(modeldressSpinnerAdapter);
    }

    public boolean modalvalida() {
        /* if (txt_subcat.getText().toString().contains(getResources().getString(R.string.select_sub_cat1))) {
            Toast.makeText(getActivity(), "" + getString(R.string.select_sub_cat), Toast.LENGTH_SHORT).show();
            return false;
        } else*/ if (edit_firstname.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.firstname), Toast.LENGTH_SHORT).show();
            edit_firstname.requestFocus();
            return false;
        } else if (edit_altemail.getText().toString().trim().length() > 0 && !Common.isValidEmail(edit_altemail.getText().toString().trim())) {
            ToastMsg.showShortToast(getActivity(), getResources().getString(R.string.email_validation));
            return false;
        } else if (edit_lastname.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.lastname), Toast.LENGTH_SHORT).show();
            edit_lastname.requestFocus();
            return false;
        } else if (edit_dob.getText().toString().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.enterdob), Toast.LENGTH_SHORT).show();
            return false;
        } else if (gen_list.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "" + getString(R.string.selectgender), Toast.LENGTH_SHORT).show();
            return false;
        } else if (edit_city.getText().toString().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.entercity), Toast.LENGTH_SHORT).show();
            edit_city.requestFocus();
            return false;
        } else if (nationality.getText().toString().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.entercountry), Toast.LENGTH_SHORT).show();
            nationality.requestFocus();
            return false;
        } else if (!confirmaage.isChecked()) {
            Toast.makeText(getActivity(), "" + getString(R.string.agecheck), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!checkterms.isChecked()) {
            Toast.makeText(getActivity(), "" + getString(R.string.enterterms), Toast.LENGTH_SHORT).show();
            return false;
        } else if (nationality_spn.getSelectedItemPosition() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.select_nationality), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public boolean companyvalida() {
        if (edit_companyname.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.companyname), Toast.LENGTH_SHORT).show();
            return false;
        } else if (alternate_email_id.getText().toString().trim().length() > 0 &&!Common.isValidEmail(alternate_email_id.getText().toString().trim())) {
            ToastMsg.showShortToast(getActivity(), getResources().getString(R.string.email_validation));
            return false;
        }else if (edit_city.getText().toString().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.entercity), Toast.LENGTH_SHORT).show();
            return false;
        } else if (nationality.getText().toString().length() == 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.entercountry), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!checkterms.isChecked()) {
            Toast.makeText(getActivity(), "" + getString(R.string.enterterms), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!web_url.getText().toString().matches(websitepattern) && web_url.getText().toString().trim().length() > 0) {
            Toast.makeText(getActivity(), "" + getString(R.string.enterurl), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public void addfragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragment.getTag());
        fragmentTransaction.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = (String) parent.getItemAtPosition(position);
        edit_city.setText(str.split(",")[0]);
        nationality.setText(str.split(",")[1]);
    }

    public boolean onsDateSet(int year, int month, int day) {
        Calendar userAge = new GregorianCalendar(year, month, day);
        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        if (minAdultAge.before(userAge)) {
            return false;
        } else {
            return true;
        }
    }

    public Dialog spinner(final Context ctx, ArrayList<CategoryModel> message, View.OnClickListener onClickListener) {
        final BottomSheetDialog MessagePopup = new BottomSheetDialog(ctx);
        MessagePopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MessagePopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        MessagePopup.setContentView(R.layout.bottom_sheet_dialog);
        MessagePopup.setCancelable(false);
        MessagePopup.setCanceledOnTouchOutside(false);
        MessagePopup.show();
        done = MessagePopup.findViewById(R.id.done);
        category_name_spntwo = MessagePopup.findViewById(R.id.category_name_spntwo);
        category_name_spntwo.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,
                false));
        adapter = new SubCategoryAdapter(getActivity(), message);
        category_name_spntwo.setAdapter(adapter);
        adapter.setItemClick(this);
        done.setOnClickListener(this);
        return MessagePopup;
    }

    @Override
    public void onItemClick(int position) {
        if (position > 0) {
            CategoryModel model = list.get(position);
            if (model.isStatus()) {
                model.setStatus(false);
                ids_list.remove(list.get(position).getC_id());
                names_list.remove(list.get(position).getC_name());
            } else {
                model.setStatus(true);
                ids_list.add(list.get(position).getC_id());
                names_list.add(list.get(position).getC_name());
            }
            list.set(position, model);
            adapter.notifyDataSetChanged();
        }
    }

    private String getIds(ArrayList<String> ids_list) {
        categoryID = "";
        categoryName = "";
        for (int i = 0; i < ids_list.size(); i++) {
            categoryID = categoryID + ids_list.get(i) + ",";
            categoryName = categoryName + names_list.get(i) + ",";
        }
        if (categoryID != null && categoryID.length() > 0)
            categoryID = categoryID.substring(0, categoryID.length() - 1);
        if (categoryName != null && categoryName.length() > 0)
            categoryName = categoryName.substring(0, categoryName.length() - 1);
        return categoryName;
    }

    public void resetdata() {
        shoe_id = 0;
        chest_id = 0;
        collar_id = 0;
        dress_id = 0;
        eyes_id = 0;
        hair_id = 0;
        height_id = 0;
        hips_id = 0;
        jacket_id = 0;
        trousers_id = 0;
        waist_id = 0;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (!((BaseActivity) getActivity()).getLanguageValue().equalsIgnoreCase(language_spn.getSelectedItem().toString())) {
            LocaleManager.setNewLocale(getActivity(), position == 1 ? LocaleManager.LANGUAGE_PERSIAN
                    : position == 2 ? LocaleManager.LANGUAGE_RUSSIAN
                    : LocaleManager.LANGUAGE_ENGLISH);
        }
        ((BaseActivity) getActivity()).setLanguageValue(position == 1 ? " Português" : position == 2 ? " Pусский" : " English");
        sharedPref.setLanguageVal(position == 1 ? "pt" : position == 2 ? "ru" : "en");
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override

        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index).toString();
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {

                        resultList = autocomplete(constraint.toString());

                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
}

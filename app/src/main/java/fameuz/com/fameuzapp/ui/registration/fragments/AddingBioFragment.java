package fameuz.com.fameuzapp.ui.registration.fragments;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.registration.activity.SubRegistrationActivity;
import fameuz.com.fameuzapp.utils.Constants;
import fameuz.com.fameuzapp.utils.ToastMsg;
import retrofit2.Response;


public class AddingBioFragment extends Fragment implements APIServerResponse {
    EditText boi_et;
    Button continue_btn, upload_later_btn;
    JsonObject jsonObject;
    SharedPref sharedPref;
    boolean showtoolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adding_bio, container, false);
        sharedPref = new SharedPref(getActivity());
        if (getArguments() != null) {
            showtoolbar = getArguments().getBoolean("isvalid");
        }
        init(view);
        clicks();
        return view;
    }

    private void clicks() {

        upload_later_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opendialogue();
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boi_et.getText().length() != 0) {
                    hit_continue();
                } else {
                    Toast.makeText(getActivity(), "" + getString(R.string.enter_about_me), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void init(View view) {
        boi_et = view.findViewById(R.id.boi_et);
        continue_btn = view.findViewById(R.id.continue_btn);
        upload_later_btn = view.findViewById(R.id.upload_later_btn);
    }


    public void addfragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragment.getTag());
        fragmentTransaction.commit();
    }

    public void opendialogue() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(getActivity().getResources().getString(R.string.upload_bio))
                .setMessage(getActivity().getResources().getString(R.string.skip_conf))
                .setPositiveButton(R.string.skip, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(sharedPref.getIsonetime().equalsIgnoreCase("true"))
                            ((BaseActivity) getActivity()).onBackPressed();
                        else
                            ((SubRegistrationActivity) getActivity()).replaceFragment(3);
                    }
                }).show();
    }

    public void hit_continue() {
        //addfragment(new UploadPhotoFragment());
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().uploadbio(APIServerResponse.UPLOAD_BIO, sharedPref.getApi_key(), sharedPref.getLanguageVal(), boi_et.getText().toString(), this);

        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        Log.e("tag", tag + "tag " + (JsonObject) response.body());
        ((BaseActivity) getActivity()).hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.UPLOAD_BIO:
                        jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("success").getAsBoolean()) {
                            if (jsonObject.get("success").getAsBoolean()) {
                                Toast.makeText(getActivity(), "" + jsonObject.get("message"), Toast.LENGTH_SHORT).show();
                                if(sharedPref.getIsonetime().equalsIgnoreCase("true"))
                                    ((BaseActivity) getActivity()).onBackPressed();
                                else
                                ((SubRegistrationActivity) getActivity()).replaceFragment(3);
                            } else {
                                ToastMsg.showLongToast(getContext(), jsonObject.get("message").getAsString());
                            }
                        }
                        break;
                }
            } catch (Exception ex) {

            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onSuccessString(int tag, String response) {

    }


    @Override
    public void onResume() {
        super.onResume();
        if (showtoolbar) {
            Common.toolbar_status(getActivity(), false, "" + getActivity().getString(R.string.addbio));
        }
    }

}

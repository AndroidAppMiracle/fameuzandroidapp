package fameuz.com.fameuzapp.ui.jobs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import fameuz.com.fameuzapp.Interfaces.OnItemClickListener;
import fameuz.com.fameuzapp.Models.CategoryModel;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.common.adapter.CategorySubSpinnerAdapter;
import fameuz.com.fameuzapp.ui.registration.adapters.SubCategoryAdapter;
import fameuz.com.fameuzapp.utils.CommonUtils;
import fameuz.com.fameuzapp.utils.ToastMsg;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.M)
public class PostJobs extends Fragment implements View.OnClickListener, APIServerResponse, OnItemClickListener, AdapterView.OnItemClickListener {

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyBZAt1jK7bI7Q7_0vKS1r1rV61KoFamxsc";
    TextView txtjob_profile, txtjob_type, ebd_date, edit_startdate, txt_cat, done;
    RelativeLayout layout_jobtype, layout_jobprofile;
    LinearLayout layout_job_type, layout_job_profile, layout_paid;
    View view;
    Button btn_nxt, btn_post;
    RecyclerView category_name_spntwo;
    SubCategoryAdapter adapter;
    AutoCompleteTextView job_city;
    EditText job_title, job_state, job_country, job_company_name, job_skills, job_desc, job_payment;
    JsonObject jsonObject;
    ArrayList<CategoryModel> list = new ArrayList<>();
    HashMap<String, String> arrhashing;
    CategorySubSpinnerAdapter customAdapter;
    Spinner select_paid, select_curency;
    int dates = 0, years = 0, mnths = 0;
    String categoryID = "", categoryName = "", payment_type = "", payment_currency = "";
    List<String> paid_selector;
    SharedPref sharedPref;
    String[] currency_arry;

    Dialog dialog1;
    ArrayList<String> ids_list = new ArrayList<>();
    ArrayList<String> names_list = new ArrayList<>();

    public static ArrayList autocomplete(String input) {

        ArrayList resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            Log.e("LOG_TAG", "Error processing Places API URL" + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("LOG_TAG", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("LOG_TAG", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("LOG_TAG", "Cannot process JSON results", e);
        }
        return resultList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.postjobs, container, false);
        ids(view);
        sharedPref = new SharedPref(getActivity());
        arrhashing = new HashMap<>();
        clicks();
        getCategories();
        onswitch(true);
        return view;
    }

    private void clicks() {
        txt_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1 = spinner(getActivity(), list, PostJobs.this);
            }
        });

        paid_selector = new ArrayList<String>();
        paid_selector.add(getActivity().getResources().getString(R.string.pls_slct));
        paid_selector.add(getActivity().getResources().getString(R.string.paid));
        paid_selector.add(getActivity().getResources().getString(R.string.unpaid));
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, paid_selector);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_paid.setAdapter(genderAdapter);

        select_paid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    if (position == 1) {
                        layout_paid.setVisibility(View.VISIBLE);
                        payment_type = "Paid";
                    } else {
                        layout_paid.setVisibility(View.GONE);
                        payment_type = "Unpaid";
                    }
                } else {
                    layout_paid.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, currency_arry);
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_curency.setAdapter(currencyAdapter);
        select_curency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("curent ", "curen " + currency_arry[position]);
                payment_currency = currency_arry[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void ids(View view) {
        txt_cat = view.findViewById(R.id.txt_cat);
        txtjob_profile = view.findViewById(R.id.txtjob_profile);
        txtjob_type = view.findViewById(R.id.txtjob_type);
        layout_jobtype = view.findViewById(R.id.layout_jobtype);
        btn_post = view.findViewById(R.id.btn_post);
        btn_nxt = view.findViewById(R.id.btn_nxt);
        layout_jobprofile = view.findViewById(R.id.layout_jobprofile);
        ebd_date = view.findViewById(R.id.ebd_date);
        edit_startdate = view.findViewById(R.id.edit_startdate);
        job_title = view.findViewById(R.id.job_title);
        layout_paid = view.findViewById(R.id.layout_paid);
        job_city = view.findViewById(R.id.job_city);
        job_state = view.findViewById(R.id.job_state);
        job_country = view.findViewById(R.id.job_country);
        job_skills = view.findViewById(R.id.job_skills);
        job_desc = view.findViewById(R.id.job_desc);
        select_paid = view.findViewById(R.id.select_paid);
        job_company_name = view.findViewById(R.id.job_company_name);
        job_payment = view.findViewById(R.id.job_payment);
        layout_job_type = view.findViewById(R.id.layout_job_type);
        layout_job_profile = view.findViewById(R.id.layout_job_profile);
        select_curency = view.findViewById(R.id.select_curency);
        btn_post.setOnClickListener(this);
        layout_jobtype.setOnClickListener(this);
        btn_nxt.setOnClickListener(this);
        layout_jobprofile.setOnClickListener(this);
        edit_startdate.setOnClickListener(this);
        ebd_date.setOnClickListener(this);
        job_city.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
        job_city.setOnItemClickListener(this);

        currency_arry =new String[] {getActivity().getResources().getString(R.string.select_curr),
                "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM",
                "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BTN",
                "BWP", "BYR", "BZD", "CAD", "CDF", "CHF", "CLP", "CNY", "COP", "CRC", "CUP",
                "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "EUR", "FJD",
                "FKP", "GBP", "GEL", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL",
                "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "IQD", "IRR", "ISK", "JMD", "JOD",
                "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK",
                "LBP", "LKR", "LRD", "LSL", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT",
                "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR", "MZN", "NAD", "NGN", "NIO",
                "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG",
                "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD",
                "SHP", "SLL", "SOS", "SRD", "SSP", "STD", "SYP", "SZL", "THB", "TJS", "TMT",
                "TND", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS",
                "VEF", "VND", "VUV", "WST", "XAF", "XCD", "XOF", "XPF", "YER", "ZAR", "ZMW",
                "ZWL"};
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_post:
                Common.hideKeyboard(getActivity(),btn_nxt);
                if (validate_jobprofile() && validate_jobtype()) {
                    hitapi(payment_type, payment_currency);
                }
                break;
            case R.id.btn_nxt:
                Common.hideKeyboard(getActivity(),btn_nxt);
                if (validate_jobprofile()) {
                    onswitch(false);
                }
                break;
            case R.id.layout_jobprofile:
                if (validate_jobtype()) {
                    onswitch(true);
                }
                break;
            case R.id.layout_jobtype:
                if (layout_job_type.getVisibility() == View.VISIBLE) {
                    if (validate_jobprofile()) {
                        onswitch(false);
                    }
                }
                break;
            case R.id.edit_startdate:
                datepicker(edit_startdate);
                break;

            case R.id.ebd_date:
                datepicker(ebd_date);
                break;

            case R.id.done:
                if (dialog1 != null && dialog1.isShowing())
                    dialog1.dismiss();
                txt_cat.setText(ids_list.size()>0?getIds(ids_list):getActivity().getResources().getString(R.string.select_category));
                break;
        }
    }

    private void hitapi(String select_paid, String currency) {
        Common.showprogress(getActivity());
        RequestBody requestBody = new FormBody.Builder()
                .add("job_title", job_title.getText().toString())
                .add("job_city", job_city.getText().toString())
                .add("job_state", job_state.getText().toString())
                .add("job_country", job_country.getText().toString())
                .add("job_company", job_company_name.getText().toString())
                .add("job_start", edit_startdate.getText().toString())
                .add("job_end", ebd_date.getText().toString())
                .add("job_type", select_paid)
                .add("job_currency", currency)
                .add("job_payment", job_payment.getText().toString())
                .add("job_skill", job_skills.getText().toString())
                .add("job_category", categoryID)
                .add("job_descr", job_desc.getText().toString())
                .build();
        ServerAPI.postapi(APIServerResponse.POST_JOB, requestBody, sharedPref.getLanguageVal(),sharedPref.getApi_key(), this);
    }

    private boolean validate_jobtype() {

        if (payment_type.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.select_payment_type), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (payment_type.equalsIgnoreCase("paid")) {
                if (job_payment.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.enter_payment), Toast.LENGTH_SHORT).show();
                    return false;
                } else if (payment_currency.equalsIgnoreCase(getActivity().getResources().getString(R.string.select_curr))) {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.select_currency), Toast.LENGTH_SHORT).show();
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
    }

    private boolean validate_jobprofile() {
        if (job_title.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.enter_job_tittle), Toast.LENGTH_SHORT).show();
            return false;
        } else if (job_city.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.enter_job_city), Toast.LENGTH_SHORT).show();
            return false;
        } else if (job_state.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.enter_job_state), Toast.LENGTH_SHORT).show();
            return false;
        } else if (job_country.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.enter_job_country), Toast.LENGTH_SHORT).show();
            return false;
        } else if (job_company_name.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.enter_company_name), Toast.LENGTH_SHORT).show();
            return false;
        } else if (edit_startdate.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.job_strt), Toast.LENGTH_SHORT).show();
            return false;
        } else if (ebd_date.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.job_end), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!CommonUtils.compareDates(edit_startdate.getText().toString().trim(),ebd_date.getText().toString().trim())) {
            Toast.makeText(getActivity(),"Please select valid start date", Toast.LENGTH_SHORT).show();
            return false;
        }else if (txt_cat.getText().toString().equalsIgnoreCase(getActivity().getResources().getString(R.string.select_category))) {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.enter_category), Toast.LENGTH_SHORT).show();
            return false;
        } else if (job_skills.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.job_skill), Toast.LENGTH_SHORT).show();
            return false;
        } else if (job_desc.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.job_descp), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public void onswitch(boolean isjobtype) {
        if (isjobtype) {
            layout_job_type.setVisibility(View.GONE);
            layout_job_profile.setVisibility(View.VISIBLE);
            txtjob_profile.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            txtjob_type.setTextColor(getActivity().getResources().getColor(R.color.black));
        } else {
            layout_job_profile.setVisibility(View.GONE);
            layout_job_type.setVisibility(View.VISIBLE);
            txtjob_profile.setTextColor(getActivity().getResources().getColor(R.color.black));
            txtjob_type.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
        }
    }

    private void datepicker(TextView textview) {

        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar selectedCal = Calendar.getInstance();
                selectedCal.set(year, monthOfYear, dayOfMonth);

                long selectedMilli = selectedCal.getTimeInMillis();
                Date datePickerDate = new Date(selectedMilli);
                if (datePickerDate.before(new Date()) || datePickerDate.equals(new Date())) {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.valid_date), Toast.LENGTH_SHORT).show();
                } else {
                    years = year;
                    mnths = monthOfYear;
                    dates = dayOfMonth;
                    String date = String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1)
                            + "-" + String.valueOf(dayOfMonth);
                    textview.setText(date);
                }
            }
        }, yy, mm, dd);
        if (years != 0) {
            datePicker.updateDate(years, mnths, dates);
        }
        datePicker.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String str = (String) parent.getItemAtPosition(position);
        List<String> list = Arrays.asList(str.split(","));
        if (list.size() == 2) {
            job_city.setText(str.split(",")[0]);
            job_state.setText(str.split(",")[0]);
            job_country.setText(str.split(",")[1]);
        } else {
            job_city.setText(str.split(",")[0]);
            job_state.setText(str.split(",")[1]);
            job_country.setText(str.split(",")[2]);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            switch (tag) {
                case APIServerResponse.GET_ALL_CATEGORIES:
                    jsonObject = (JsonObject) response.body();
                    if (jsonObject.get("success").getAsBoolean()) {
                        if (jsonObject.get("success").getAsBoolean()) {
                            categoriespineer(jsonObject.get("categories").getAsJsonArray());

                        } else {
                            ToastMsg.showLongToast(getContext(), jsonObject.get("message").getAsString());
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
    }

    @Override
    public void onSuccessString(int tag, String response) {
        Common.dismissprogress();
        try {
            JSONObject jsonObject = new JSONObject(response);
            switch (tag) {
                case APIServerResponse.POST_JOB:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "" + jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getCategories() {
        ServerAPI.getInstance().getAllCategories(APIServerResponse.GET_ALL_CATEGORIES, sharedPref.getLanguageVal(),
                "0", this);
    }

    private void categoriespineer(JsonArray categories) {
        JsonArray jsonArray = jsonObject.get("categories").getAsJsonArray();
        if (list.size() > 0)
            list.clear();
        CategoryModel modelas = new CategoryModel();
        modelas.setC_name(getActivity().getResources().getString(R.string.select_category));
        modelas.setC_id("0");
        list.add(modelas);
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject obj = jsonArray.get(i).getAsJsonObject();
            CategoryModel model = new CategoryModel();
            model.setC_id(obj.get("c_id").getAsString());
            model.setC_name(obj.get("c_name").getAsString());
            model.setStatus(false);
            list.add(model);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Common.toolbar_status(getActivity(), false, "" + getActivity().getString(R.string.postjobs));
    }

    public Dialog spinner(final Context ctx, ArrayList<CategoryModel> message, View.OnClickListener onClickListener) {
        final BottomSheetDialog MessagePopup = new BottomSheetDialog(ctx);
        MessagePopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MessagePopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        MessagePopup.setContentView(R.layout.bottom_sheet_dialog);
        MessagePopup.setCancelable(false);
        MessagePopup.setCanceledOnTouchOutside(false);
        MessagePopup.show();

        done = MessagePopup.findViewById(R.id.done);
        category_name_spntwo = MessagePopup.findViewById(R.id.category_name_spntwo);
        category_name_spntwo.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,
                false));
        adapter = new SubCategoryAdapter(getActivity(), message);
        category_name_spntwo.setAdapter(adapter);
        adapter.setItemClick(this);

        done.setOnClickListener(this);

        return MessagePopup;
    }

    @Override
    public void onItemClick(int position) {
        if (position > 0) {
            CategoryModel model = list.get(position);
            if (model.isStatus()) {
                model.setStatus(false);
                ids_list.remove(list.get(position).getC_id());
                names_list.remove(list.get(position).getC_name());
            } else {
                model.setStatus(true);
                ids_list.add(list.get(position).getC_id());
                names_list.add(list.get(position).getC_name());
            }
            list.set(position, model);
            adapter.notifyDataSetChanged();
        }
    }

    private String getIds(ArrayList<String> ids_list) {
        categoryID = "";
        categoryName = "";
        for (int i = 0; i < ids_list.size(); i++) {
            categoryID = categoryID + ids_list.get(i) + ",";
            categoryName = categoryName + names_list.get(i) + ",";
        }

        if (categoryID != null && categoryID.length() > 0)
            categoryID = categoryID.substring(0, categoryID.length() - 1);

        if (categoryName != null && categoryName.length() > 0)
            categoryName = categoryName.substring(0, categoryName.length() - 1);

        Log.e("sdfsdf", "sdfdsfsdf" + categoryID + "\n" + categoryName);
        return categoryName;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override

        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index).toString();
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        resultList = autocomplete(constraint.toString());
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
}

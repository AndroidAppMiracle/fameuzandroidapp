package fameuz.com.fameuzapp.ui.common;

import java.io.Serializable;
import java.util.ArrayList;

import fameuz.com.fameuzapp.Models.RelatedJobsPojo;

public class RelatedJobSerial implements Serializable {

    private static final long serialVersionUID = -7060210544600464481L;


    public ArrayList<RelatedJobsPojo> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<RelatedJobsPojo> arrayList) {
        this.arrayList = arrayList;
    }

    private ArrayList<RelatedJobsPojo> arrayList;


}

package fameuz.com.fameuzapp.ui.common.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.List;

import fameuz.com.fameuzapp.Models.CategoryModel;

public class CategorySubSpinnerAdapter extends BaseAdapter implements SpinnerAdapter{
    Activity context;
    List<CategoryModel> list=new ArrayList<>();

    public CategorySubSpinnerAdapter(Activity context, List<CategoryModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(context);
      /*  txt.setGravity(Gravity.CENTER);*/
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setText(list.get(i).getC_name());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(list.get(position).getC_name());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }

}

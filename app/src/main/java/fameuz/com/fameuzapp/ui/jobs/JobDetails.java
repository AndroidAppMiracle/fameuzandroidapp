package fameuz.com.fameuzapp.ui.jobs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fameuz.com.fameuzapp.Models.RelatedJobsPojo;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.retrofit.model.JobDetailsPojo;
import fameuz.com.fameuzapp.ui.common.RelatedJobSerial;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import retrofit2.Response;

public class JobDetails extends Fragment implements APIServerResponse {

    View view;
    SharedPref sharedPref;
    TextView job_location, job_category, job_date, job_company, job_payment, job_skills, job_postedate, job_description,job_name;
    JSONObject jsonObject;
    TextView txt_relatedjob;
    boolean addbackstack;
    String job_id, link;
    LinearLayout layout_share;
    Button btn_apply_forjob;
    ArrayList<JobDetailsPojo> jobDetailsPojos;
    ArrayList<RelatedJobsPojo> relatedJobsPojos;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.job_details, container, false);
        sharedPref = new SharedPref(getActivity());
        job_id = getArguments().getString("id");
        if (getArguments() != null) {
            addbackstack = getArguments().getBoolean("add");
        }
        ids(view);
        Common.showprogress(getActivity());
        getCategories();
        return view;
    }

    private void ids(View view) {
        job_name= view.findViewById(R.id.job_name);
        job_location = view.findViewById(R.id.job_location);
        job_category = view.findViewById(R.id.job_category);
        job_date = view.findViewById(R.id.job_date);
        job_company = view.findViewById(R.id.job_company);
        job_payment = view.findViewById(R.id.job_payment);
        job_skills = view.findViewById(R.id.job_skills);
        job_postedate = view.findViewById(R.id.job_postedate);
        job_description = view.findViewById(R.id.job_description);
        btn_apply_forjob = view.findViewById(R.id.btn_apply_forjob);
        txt_relatedjob = view.findViewById(R.id.txt_relatedjob);
        layout_share = view.findViewById(R.id.layout_share);
        clicks();
    }

    private void clicks() {
        btn_apply_forjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPref.getIs_user_verified().equalsIgnoreCase("true")) {
                    try {
                        if (jsonObject.optJSONObject("data").optBoolean("apply_status")==true
                                && btn_apply_forjob.getText().toString().equalsIgnoreCase(getActivity().getString(R.string.already_applied_text))){
                            Toast.makeText(getActivity(), ""+getActivity().getString(R.string.already_applied), Toast.LENGTH_SHORT).show();
                        }else{
                            Common.showprogress(getActivity());
                            postjobdetail();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    Toast.makeText(getActivity(), "" + getActivity().getResources().getString(R.string.user_not_verified), Toast.LENGTH_SHORT).show();

            }
        });


        layout_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPref.getIs_user_verified().equalsIgnoreCase("true"))
                Common.shareintent(getActivity(), link);
                else  {
                    Toast.makeText(getActivity(), "" + getActivity().getResources().getString(R.string.user_not_verified), Toast.LENGTH_SHORT).show();
                }

            }
        });

        txt_relatedjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(relatedJobsPojos.size()==0){
                    Toast.makeText(getActivity(), ""+getString(R.string.nojobsfound), Toast.LENGTH_SHORT).show();
                }else {
                    RelatedJobSerial relatedJobSerial = new RelatedJobSerial();
                    relatedJobSerial.setArrayList(relatedJobsPojos);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("related_job", relatedJobSerial);
                    Common.addfragmentwithbundle(getActivity(), new RelatedJobs(), bundle, addbackstack);
                }
                // getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    public void getCategories() {
        ServerAPI.getInstance().getjobdetails(APIServerResponse.GET_JOB_DETAILS, sharedPref.getApi_key(),
                sharedPref.getLanguageVal(), job_id, this);
    }

    public void postjobdetail() {
        ServerAPI.getInstance().applyforjob(APIServerResponse.APPLYFORPOST,sharedPref.getLanguageVal(),
                sharedPref.getApi_key(), job_id, this);
    }

    @Override
    public void onSuccess(int tag, Response response) {

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        //   ((BaseActivity) getActivity()).hideLoading();
        Common.dismissprogress();
    }

    @Override
    public void onSuccessString(int tag, String response) {
        Log.e("tag", tag + "tag " + response);
        Common.dismissprogress();
        try {
            switch (tag) {
                case APIServerResponse.GET_JOB_DETAILS:
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optBoolean("success")) {
                        jobDetailsPojos = new ArrayList<>();
                        if(!btn_apply_forjob.getText().toString().equalsIgnoreCase(getActivity().getString(R.string.already_applied_text)))
                        setdata(jsonObject);
                    } else {
                        Toast.makeText(getActivity(), "" + jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    }
                    Common.toolbar_status(getActivity(), false, "" +jsonObject.getJSONObject("data").optString("job_title"));
                    break;

                case APIServerResponse.APPLYFORPOST:
                    btn_apply_forjob.setText(getActivity().getString(R.string.already_applied_text));
                    getCategories();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setdata(JSONObject data) {
        try {
            job_location.setText(data.getJSONObject("data").optString("job_city")+", "+data.getJSONObject("data").optString("job_country"));
            job_category.setText("" + data.getJSONObject("data").optString("job_category"));
            job_date.setText(data.getJSONObject("data").optString("job_start") + " to \n" + data.getJSONObject("data").optString("job_end"));
            job_company.setText("" + data.getJSONObject("data").optString("job_company"));
            job_payment.setText(data.getJSONObject("data").optString("job_currency") + " " + data.getJSONObject("data").optString("job_payment"));
            job_skills.setText("" + data.getJSONObject("data").optString("job_skill"));
            job_postedate.setText("" + data.getJSONObject("data").optString("job_created"));
            job_description.setText("" + data.getJSONObject("data").optString("job_descr"));
            link = data.getJSONObject("data").optString("shareLink");
            if (data.getJSONObject("data").getBoolean("apply_status")==true &&
                    !data.getJSONObject("data").getString("apply_message").equalsIgnoreCase(getString(R.string.apply))) {
                btn_apply_forjob.setVisibility(View.VISIBLE);
                btn_apply_forjob.setText(getString(R.string.already_applied_text));
            } else if(data.getJSONObject("data").optString("user_id").equalsIgnoreCase(sharedPref.getUserid())){
                btn_apply_forjob.setVisibility(View.GONE);
            }else
                btn_apply_forjob.setVisibility(View.VISIBLE);

            relatedJobsPojos = new ArrayList<>();
            JSONArray jsonArray = data.optJSONObject("relatedJobs").optJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                RelatedJobsPojo relatedJobsPojo = new RelatedJobsPojo();
                relatedJobsPojo.setIsApproved(jsonArray.getJSONObject(i).optString("is_approved"));
                relatedJobsPojo.setJobTitle(jsonArray.getJSONObject(i).optString("job_title"));
                relatedJobsPojo.setJobDescr(jsonArray.getJSONObject(i).optString("job_descr"));
                relatedJobsPojo.setJobState(jsonArray.getJSONObject(i).optString("job_state"));
                relatedJobsPojo.setJobCity(jsonArray.getJSONObject(i).optString("job_city"));
                relatedJobsPojo.setJobCountry(jsonArray.getJSONObject(i).optString("job_country"));
                relatedJobsPojo.setJobId(jsonArray.getJSONObject(i).optString("job_id"));
                relatedJobsPojo.setJobImage(jsonArray.getJSONObject(i).getJSONObject("user").optString("profile_image"));
                relatedJobsPojo.setJobCreated(jsonArray.getJSONObject(i).optString("job_created"));
                relatedJobsPojo.setJobEdited(jsonArray.getJSONObject(i).optString("job_edited"));
                relatedJobsPojo.setJobEnd(jsonArray.getJSONObject(i).optString("job_end"));
                relatedJobsPojo.setJobStart(jsonArray.getJSONObject(i).optString("job_start"));
                relatedJobsPojos.add(relatedJobsPojo);
            }
            Log.e("relatedJobsPojos", "relatedJobsPojos " + relatedJobsPojos.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}


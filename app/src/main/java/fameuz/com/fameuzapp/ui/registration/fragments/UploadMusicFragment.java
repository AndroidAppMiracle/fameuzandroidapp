package fameuz.com.fameuzapp.ui.registration.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.Crop_Activity;
import fameuz.com.fameuzapp.common.GlobalConstant;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.navigationdrawer.activity.MainActivity;
import fameuz.com.fameuzapp.utils.CommonUtils;
import fameuz.com.fameuzapp.utils.Constants;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Response;

public class UploadMusicFragment extends Fragment implements APIServerResponse {

    private static final int REQ_CODE_PICK_SOUNDFILE = 200;
    View view;
    File music_file = null, user_dp = null;
    SharedPref sharedPref;
    CheckBox check_allowmusic;
    TextView txt_chosefile, txt_uploadurl, txt_choseimgfile;
    EditText edit_uploadmusic, edit_uploadmusicdes;
    Button uploadmusicbtn, continue_btnfinish, upload_later_btnas, uploadimgbtn;
    boolean showtoolbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.uploadmusic, container, false);
        sharedPref = new SharedPref(getActivity());
        if (getArguments() != null) {
            showtoolbar = getArguments().getBoolean("isvalid");
        }
        ids(view);
        clicks();
        return view;
    }

    private void ids(View view) {
        edit_uploadmusic = view.findViewById(R.id.edit_uploadmusic);
        edit_uploadmusicdes = view.findViewById(R.id.edit_uploadmusicdes);
        check_allowmusic = view.findViewById(R.id.check_allowmusic);
        txt_chosefile = view.findViewById(R.id.txt_chosefile);
        uploadmusicbtn = view.findViewById(R.id.uploadmusicbtn);
        txt_uploadurl = view.findViewById(R.id.txt_uploadurl);
        upload_later_btnas = view.findViewById(R.id.upload_later_btnas);
        continue_btnfinish = view.findViewById(R.id.continue_btnfinish);
        txt_choseimgfile = view.findViewById(R.id.txt_choseimgfile);
        uploadimgbtn = view.findViewById(R.id.uploadimgbtn);
    }

    public void clicks() {
        uploadmusicbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/*");
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), REQ_CODE_PICK_SOUNDFILE);
            }
        });
        continue_btnfinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_dp != null) {
                    if (check_allowmusic.isChecked())
                        hit_continue("1");
                    else
                        hit_continue("0");
                } else {
                    Toast.makeText(getActivity(), "" + getString(R.string.uploadmusicfile), Toast.LENGTH_SHORT).show();
                }
            }
        });
        upload_later_btnas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });
        uploadimgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = {getActivity().getResources().getString(R.string.take_photo), getActivity().getResources().getString(R.string.select_gallery),
                getActivity().getResources().getString(R.string.cancel)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getResources().getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = CommonUtils.checkPermission(getContext());
                if (item==0) {
                    if (result)
                        takePhoto();
                } else if (item==1) {
                    if (result)
                        selectPhoto();
                } else if (item==2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void takePhoto() {
        EasyImage.openCamera(this, 0);
    }

    public void selectPhoto() {
        EasyImage.openGallery(this, 0);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        ((BaseActivity) getActivity()).hideLoading();
        switch (tag) {
            case APIServerResponse.UPLOAD_MUSIC:
                getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                break;

        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        ((BaseActivity) getActivity()).hideLoading();

    }

    @Override
    public void onSuccessString(int tag, String response) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_CODE_PICK_SOUNDFILE) {
                if ((data != null) && (data.getData() != null)) {
                    Uri audioFileUri = data.getData();
                    String selectedPath = Common.getPath(getActivity(), audioFileUri);
                    music_file = new File(selectedPath);
                    txt_chosefile.setText(selectedPath);
                }
            }
            if (requestCode == 105) {
                if (GlobalConstant.Photo != null) {
                    user_dp = persistImage(GlobalConstant.Photo);
                    txt_choseimgfile.setText(user_dp.getAbsolutePath());
                } else {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.failupload), Toast.LENGTH_SHORT).show();
                }
            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                getUriFromFile(imageFiles.get(0));
            }
        });
    }

    private void getUriFromFile(File imageFile) {
        Uri uri = Uri.fromFile(new File(imageFile.getAbsolutePath()));
        startActivityForResult(new Intent(getActivity(), Crop_Activity.class)
                .putExtra("imageUri", uri.toString()), 105);
    }

    private File persistImage(Bitmap bitmap) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
        Date now = new Date();
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, formatter.format(now) + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }

    public void hit_continue(String s) {
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            Log.e("user_dp", "user_dp" + user_dp);
            ServerAPI.getInstance().showmusic(APIServerResponse.UPLOAD_MUSIC,sharedPref.getLanguageVal(), sharedPref.getApi_key(), music_file, user_dp,
                    edit_uploadmusic.getText().toString(), edit_uploadmusicdes.getText().toString(), s, this);
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (showtoolbar) {
            Common.toolbar_status(getActivity(), false, "" + getActivity().getString(R.string.uploadmusic));
        }
    }
}

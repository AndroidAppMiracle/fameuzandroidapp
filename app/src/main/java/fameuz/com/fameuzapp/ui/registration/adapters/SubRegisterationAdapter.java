package fameuz.com.fameuzapp.ui.registration.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import fameuz.com.fameuzapp.ui.registration.fragments.AddingBioFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.BasicDetailFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadMusicFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadPhotoFragment;
import fameuz.com.fameuzapp.ui.registration.fragments.UploadShowReelFragment;

public class SubRegisterationAdapter extends FragmentPagerAdapter {
    //integer to count number of tabs
    int tabCount;
    Context mContext;

    public SubRegisterationAdapter(FragmentManager fm, int tabCount, Context mContext) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
        this.mContext = mContext;

    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new BasicDetailFragment();
                return fragment;
            case 1:
                fragment = new UploadPhotoFragment();
                return fragment;
            case 2:
                fragment = new AddingBioFragment();
                return fragment;
            case 3:
                fragment = new UploadShowReelFragment();
                return fragment;
            default:
                fragment = new BasicDetailFragment();
                return fragment;

        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    public Fragment getFragment(ViewPager container, int position, FragmentManager fm) {
        String name = makeFragmentName(container.getId(), position);
        return fm.findFragmentByTag(name);
    }

    private String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }
}

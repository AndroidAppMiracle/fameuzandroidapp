package fameuz.com.fameuzapp.ui.registration.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.GifImageView;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.login.activity.LoginActivity;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.GlobalConstant;
import fameuz.com.fameuzapp.common.SharedPref;
import retrofit2.Response;

public class ActivityVerification extends BaseActivity implements APIServerResponse {

    TextView sasaas;
    SharedPref sharedPref;
    GifImageView gifImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        sharedPref = new SharedPref(ActivityVerification.this);
        gifImageView = findViewById(R.id.gifImageView);
        sasaas=findViewById(R.id.sasaas);

        gifImageView.setGifAssetPath(GlobalConstant.stringPath);
        sasaas.setText(getResources().getString(R.string.hello)+" "+sharedPref.getUser_name());

        findViewById(R.id.login_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.showprogress(ActivityVerification.this);
                Log.e("shared ","shared "+sharedPref.getApi_key());
                ServerAPI.getInstance().resend_verification(APIServerResponse.RESEND_VERIFICATION,sharedPref.getLanguageVal(), sharedPref.getApi_key(),ActivityVerification.this);
            }
        });


        findViewById(R.id.login_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityVerification.this, LoginActivity.class));
                   finish();
            }
        });
    }

    @Override
    public void onSuccess(int tag, Response response) {
        Common.dismissprogress();
    }

    @Override
    public void onError(int tag, Throwable throwable) {
    }

    @Override
    public void onSuccessString(int tag, String response) {
        Log.e("Res","res "+response);
        Common.dismissprogress();
        Common.dismissprogress();
        try {
            JSONObject jsonObject = new JSONObject(response);

            Toast.makeText(ActivityVerification.this, ""+jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ActivityVerification.this, LoginActivity.class));
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}

package fameuz.com.fameuzapp.ui.registration.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.retrofit.model.JobDetailsPojo;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.ui.jobs.JobDetails;

public class Adapter_SearchJob extends RecyclerView.Adapter<Adapter_SearchJob.MyViewHolder> {

    private Context activity;
    ArrayList<JobDetailsPojo> arrayList;
    FragmentManager supportFragmentManager;

    public Adapter_SearchJob(Context activity, ArrayList<JobDetailsPojo> arrayList, FragmentManager supportFragmentManager) {
        this.activity=activity;
        this.arrayList=arrayList;
        this.supportFragmentManager=supportFragmentManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView talent_img;
        LinearLayout layout_main;
        TextView viewdetails,jobtittle,job_descri,job_locations,txt_postedon,txt_jobdate,txt_expireon;
        MyViewHolder(View view) {
            super(view);
            talent_img = view.findViewById(R.id.talent_img);
            viewdetails = view.findViewById(R.id.viewdetails);
            jobtittle = view.findViewById(R.id.jobtittle);
            job_descri = view.findViewById(R.id.job_descri);
            job_locations = view.findViewById(R.id.job_locations);
            txt_jobdate = view.findViewById(R.id.txt_jobdate);
            txt_postedon = view.findViewById(R.id.txt_postedon);
            txt_expireon = view.findViewById(R.id.txt_expireon);
            layout_main = view.findViewById(R.id.layout_main);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_job, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if(arrayList.get(position).getJobImage().length()>0){
            Glide.with(activity)
                    .load(arrayList.get(position).getJobImage())
                    .into(holder.talent_img);
        }
        holder.jobtittle.setText(arrayList.get(position).getJobTitle());
        holder.job_descri.setText(arrayList.get(position).getJobCategory());
        holder.job_locations.setText(arrayList.get(position).getJobCity()+" - "+arrayList.get(position).getJobCountry());
        holder.txt_postedon.setText(arrayList.get(position).getJobCreated());
        holder.txt_jobdate.setText(arrayList.get(position).getJobStart());
        holder.txt_expireon.setText(arrayList.get(position).getJobEnd());
        holder.viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addfragment(new JobDetails(),arrayList.get(position).getJobId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void addfragment(Fragment fragment,String id){
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragment.getTag());
        fragmentTransaction.addToBackStack(fragment.getTag());
        Bundle bundle = new Bundle();
        bundle.putString("id",id);
        bundle.putBoolean("add",true);
        fragment.setArguments(bundle);
        fragmentTransaction.commit();
    }
}
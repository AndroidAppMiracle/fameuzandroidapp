package fameuz.com.fameuzapp.ui.common.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.google.gson.JsonArray;

import fameuz.com.fameuzapp.R;

public class CategorySpinnerAdapter extends BaseAdapter implements SpinnerAdapter{
    Activity context;
    JsonArray jsonArray;

    public CategorySpinnerAdapter(Activity context, JsonArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @Override
    public int getCount() {
        if (jsonArray.size() != 0) {
            return jsonArray.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return jsonArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(context);
      /*  txt.setGravity(Gravity.CENTER);*/
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setText(jsonArray.get(i).getAsJsonObject().get("c_name").getAsString());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(jsonArray.get(position).getAsJsonObject().get("c_name").getAsString());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }

}

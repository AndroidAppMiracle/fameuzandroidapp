package fameuz.com.fameuzapp.ui.registration.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.registration.activity.SubRegistrationActivity;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.Crop_Activity;
import fameuz.com.fameuzapp.common.GlobalConstant;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.utils.CommonUtils;
import fameuz.com.fameuzapp.utils.Constants;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Response;


public class UploadPhotoFragment extends Fragment implements View.OnClickListener, APIServerResponse {

    private static final int PIC_CROP = 10;
    Button upload_later_btn;
    ImageView img_uploadpic;
    File user_dp;
    Bitmap thePic;
    CheckBox check_photo;
    SharedPref sharedPref;
    boolean showtoolbar;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    public UploadPhotoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload_photo, container, false);
        sharedPref = new SharedPref(getActivity());
        if (getArguments() != null) {
            showtoolbar = getArguments().getBoolean("isvalid");
        }
        init(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (showtoolbar) {
            Common.toolbar_status(getActivity(), false, "" + getActivity().getString(R.string.upload_photo));
        }
    }

    private void init(View view) {
        //   selectedImageView = view.findViewById(R.id.image_user);
        upload_later_btn = view.findViewById(R.id.upload_later_btn);
        img_uploadpic = view.findViewById(R.id.img_uploadpic);
        check_photo = view.findViewById(R.id.check_photo);
        upload_later_btn.setOnClickListener(this);
        img_uploadpic.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_uploadpic:
                selectImage();
                break;
            case R.id.upload_later_btn:
                if (!check_photo.isChecked()) {
                    Toast.makeText(getActivity(), "" + getString(R.string.acceptance_required), Toast.LENGTH_SHORT).show();
                } else if (user_dp == null) {
                    Toast.makeText(getActivity(), "" + getString(R.string.selecteimage), Toast.LENGTH_SHORT).show();

                } else {
                    hit_continue();
                }

                break;
        }
    }


    //----------------------------------------------------------------

    private void selectImage() {
        final CharSequence[] items = {getActivity().getResources().getString(R.string.take_photo), getActivity().getResources().getString(R.string.select_gallery),
                getActivity().getResources().getString(R.string.cancel)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getResources().getString(R.string.add_a_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = CommonUtils.checkPermission(getContext());
                if (item==0) {
                    if (result)
                        takePhoto();
                } else if (item==1) {
                    if (result)
                        selectPhoto();
                } else if (item==2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 105) {
                if (GlobalConstant.Photo != null) {
                    img_uploadpic.setImageBitmap(GlobalConstant.Photo);
                    user_dp = persistImage(GlobalConstant.Photo);
                } else {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.failupload), Toast.LENGTH_SHORT).show();
                }
            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                getUriFromFile(imageFiles.get(0));
            }
        });

    }

    private void getUriFromFile(File imageFile) {
        Uri uri = Uri.fromFile(new File(imageFile.getAbsolutePath()));
        //   cropimage(uri);
        startActivityForResult(new Intent(getActivity(), Crop_Activity.class)
                .putExtra("imageUri", uri.toString()), 105);
    }

    public void cropimage(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        //indicate image type and Uri
        cropIntent.setDataAndType(picUri, "image/*");
        //set crop properties
        cropIntent.putExtra("crop", "true");
        //indicate aspect of desired crop
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        //indicate output X and Y
        cropIntent.putExtra("outputX", 256);
        cropIntent.putExtra("outputY", 256);
        //retrieve data on return
        cropIntent.putExtra("return-data", true);
        //start the activity - we handle returning in onActivityResult
        startActivityForResult(cropIntent, PIC_CROP);
    }

    @Override
    public void onSuccess(int tag, Response response) {
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onSuccessString(int tag, String response) {
        Log.e("tag", tag + " tag " + response);
        ((BaseActivity) getActivity()).hideLoading();

        switch (tag) {
            case APIServerResponse.UPLOAD_PHOTO:
                // String res =  new String(response.raw().toString());
                //  jsonObject = (JsonObject) response.raw().isSuccessful();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject detailUserObjectas = jsonObject.getJSONObject("result").getJSONObject("user");
                    if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                        sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                    } else {
                        sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                    }
                    sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                    sharedPref.setIsemail(detailUserObjectas.optString("email"));
                    if (jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                        sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                    } else {
                        sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                    }
                    sharedPref.setHasMusicCatgeory(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                    sharedPref.setIsTalent(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                    sharedPref.setIsModel(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                    sharedPref.setIsAgency(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));
                    sharedPref.setIs_userage(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("age"));
                    sharedPref.setIs_user_verified(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                            && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                            ? "true" : "false");
                    sharedPref.setUser_img(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                            && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                            ? detailUserObjectas.optString("profile_image")
                            : detailUserObjectas.optString("pendingProfileImage"));

                    if(sharedPref.getIsonetime().equalsIgnoreCase("true"))
                        ((BaseActivity) getActivity()).onBackPressed();
                    else
                    ((SubRegistrationActivity) getActivity()).replaceFragment(2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }
    public void hit_continue() {
        //addfragment(new UploadPhotoFragment());
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();

            ServerAPI.getInstance().uploadimage(APIServerResponse.UPLOAD_PHOTO, sharedPref.getLanguageVal(),sharedPref.getApi_key(), user_dp, this);

        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void takePhoto() {

        EasyImage.openCamera(this, 0);
    }

    public void selectPhoto() {
        EasyImage.openGallery(this, 0);
    }

    private File persistImage(Bitmap bitmap) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
        Date now = new Date();
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, formatter.format(now) + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }
}

package fameuz.com.fameuzapp.ui.common.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.widget.ProgressBar;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.ui.login.activity.LoginActivity;
import fameuz.com.fameuzapp.ui.navigationdrawer.activity.MainActivity;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.ui.registration.activity.SubRegistrationActivity;

public class SplashActivity extends BaseActivity{
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private int progressStatus = 0;
    SharedPref sharedPref;
    ProgressBar progressBar;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        sharedPref = new SharedPref(SplashActivity.this);
        progress();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               if(sharedPref.getIsonetime().equalsIgnoreCase("true")) {
                   Intent i = new Intent(SplashActivity.this, MainActivity.class);
                   startActivity(i);
               } else {
                   Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                   startActivity(i);
               }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


    public void progress(){
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressStatus);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

}

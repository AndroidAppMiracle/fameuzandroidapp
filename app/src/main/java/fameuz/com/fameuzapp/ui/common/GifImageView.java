package fameuz.com.fameuzapp.ui.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.net.Uri;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import java.io.FileNotFoundException;
import java.io.InputStream;


public class GifImageView  extends WebView {

    public GifImageView(Context context) {
        super(context);
    }

    public GifImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setGifAssetPath(String pPath) {
        String baseUrl = pPath.substring(0, pPath.lastIndexOf("/") + 1);
        String fileName = pPath.substring(pPath.lastIndexOf("/")+1);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style>");
        strBuilder.append("</head><body>");
        strBuilder.append("<img src=" + fileName + " width=100% /></body></html>");
        String data = strBuilder.toString();
        Log.d(this.getClass().getName(), "data: " + data);
        Log.d(this.getClass().getName(), "base url: " + baseUrl);
        Log.d(this.getClass().getName(), "file name: " + fileName);
        loadDataWithBaseURL(baseUrl, data, "text/html", "utf-8", null);
    }
}
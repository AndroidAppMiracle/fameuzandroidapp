package fameuz.com.fameuzapp.ui.common.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.utils.Constants;
import fameuz.com.fameuzapp.utils.LocaleManager;
import fameuz.com.fameuzapp.utils.ObscuredSharedPreferences;

public class BaseActivity extends AppCompatActivity {
    static ObscuredSharedPreferences prefs;
    private Snackbar snackbar;
    private AlertDialog loadingDialog;
    static DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //HH for hour of the day (0 - 23)
    static DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd h:mm a");
    private SharedPreferences mPrefs, tokenPrefs;

    public SharedPreferences getmPrefs() {
        return mPrefs;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = ObscuredSharedPreferences.getPrefs(this, getString(R.string.app_name), Context.MODE_PRIVATE);
        mPrefs = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        tokenPrefs = getSharedPreferences(Constants.DEVICE_ID, MODE_PRIVATE);
    }

    public SharedPreferences gettokenPrefs() {
        return tokenPrefs;
    }


    public static String getDeviceID(Context context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    public void setFcm(String fcmToken) {
        Constants.FCM_TOKEN = fcmToken;
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


    /**
     * For hiding the loading
     */
    public void hideLoading() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * For Hiding the keyboard
     */
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    /**
     * For Displaying the loading when a service is hit
     */
    public void showLoading() {
        try {
            if (loadingDialog == null) {
                LayoutInflater factory = LayoutInflater.from(this);
                final View deleteDialogView = factory.inflate(R.layout.layout_progress, null);
                loadingDialog = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).create();
                loadingDialog.setView(deleteDialogView);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.setCancelable(false);
            }
            loadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissDialog() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
                loadingDialog = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFirebaseToken() {

        if (gettokenPrefs().getString(Constants.FCM_TOKEN, "").equalsIgnoreCase("")) {
           // getmPrefs().edit().putString(Constants.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken()).apply();

            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        } else {
            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        }

    }

   /* public String getFirebaseToken() {

        if (gettokenPrefs().getString(Constants.FCM_TOKEN, "").equalsIgnoreCase("")) {
            getmPrefs().edit().putString(Constants.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken()).apply();
            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        } else {
            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        }

    }*/


    /**
     * function for fetching user ID
     *
     * @return user ID
     */
    public String getUserFcmToken() {
        return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
    }

    /**
     * For Displaying the snackBar
     *
     * @param msg message to display in the snackBar.
     */
    public void showSnack(String msg) {
        try {
            snackbar = Snackbar.make(getCurrentFocus(), msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }

    public void showSnack(String msg, View view) {
        try {
            snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }

    /**
     * Function to display Toast
     *
     * @param message  Message of the Toast
     * @param duration Duration of the Toast 0 for Short and 1 for Long Toast
     */
    public void showToast(String message, int duration) {
        if (message != null && message.length() > 0) {
            if (duration == 0) {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * For Displaying Output in LogCat
     *
     * @param message Message to display
     */
    public void showOutput(String message) {
        System.out.println("------------------------");
        System.out.println(message);
        System.out.println("------------------------");
    }


    /**
     * For Displaying the Log Output in Logcat
     *
     * @param tag     Tag of the Log
     * @param message Message of the Log
     */
    public void showLog(String tag, String message) {
        System.out.println("--------------------");
        Log.d(tag, message);
        System.out.println("--------------------");
    }

    /**
     * function to check if internet connection is active or not.
     *
     * @return true if connected to internet else false
     */
    public boolean isConnectedToInternet() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null;
    }


  /*  private static String eneteredNa,e = "user_id";
    private static String userIdDefValue = "";

    public void setUserID(String userIDValue) {
        prefs.edit().putString(userID, userIDValue).apply();
    }

    public String getUserID() {
        return prefs.getString(userID, userIdDefValue);
    }
    */
  private static String langauge = "language";
    private static String languageValue = "";

    public void setLanguageValue(String langaugeSt) {
        prefs.edit().putString(langauge, langaugeSt).apply();
    }

    public String getLanguageValue() {
        return prefs.getString(langauge, languageValue);
    }

    private static String userID = "user_id";
    private static String userIdDefValue = "";

    public void setUserID(String userIDValue) {
        prefs.edit().putString(userID, userIDValue).apply();
    }

    public String getUserID() {
        return prefs.getString(userID, userIdDefValue);
    }

    private static String emailId = "email_id";
    private static String emailIdDefValue = "";

    public void setEmailID(String emailIdValue) {
        prefs.edit().putString(emailId, emailIdValue).apply();
    }

    public String getEmailID() {
        return prefs.getString(emailId, emailIdDefValue);
    }


    private static String userType = "user_type";
    private static String userTypeDefValue = "";


    public void setUserType(String userTypeValue) {
        prefs.edit().putString(userType, userTypeValue).apply();
    }

    public String getUserType() {
        return prefs.getString(userType, userTypeDefValue);
    }


    private static String firstName = "first_name";
    private static String firstNameDefValue = "";


    public void setFirstName(String firstNameValue) {
        prefs.edit().putString(firstName, firstNameValue).apply();
    }

    public String getFirstName() {
        return prefs.getString(firstName, firstNameDefValue);
    }

    private static String lastName = "last_name";
    private static String lastNameDefValue = "";


    public void setLastName(String lastNameValue) {
        prefs.edit().putString(lastName, lastNameValue).apply();
    }

    public String getLastName() {
        return prefs.getString(lastName, lastNameDefValue);
    }

    private static String userName = "user_name";
    private static String userNameDefValue = "";


    public void setUserName(String userNameValue) {
        prefs.edit().putString(userName, userNameValue).apply();
    }

    public String getUserName() {
        return prefs.getString(userName, userNameDefValue);
    }


    private static String profile_image = "profile_image";
    private static String profileImage = "";


    public void setProfileUrl(String profileImage) {
        prefs.edit().putString(profile_image, profileImage).apply();
    }

    public String getProfileUrl() {
        return prefs.getString(profile_image, profileImage);
    }


    private static String phoneNumber = "phone_number";
    private static String phoneNumberDefValue = "";


    public void setPhoneNumber(String phoneNumberValue) {
        prefs.edit().putString(phoneNumber, phoneNumberValue).apply();
    }

    public String getPhoneNumber() {
        return prefs.getString(phoneNumber, phoneNumberDefValue);
    }


    private static String dob = "dob";
    private static String dobDefValue = "";


    public void setDOB(String dobValue) {
        prefs.edit().putString(dob, dobValue).apply();
    }

    public String getDOB() {
        return prefs.getString(dob, dobDefValue);
    }


    private static String gender = "gender";
    private static String genderDefValue = "";

    public static String getC_name() {
        return prefs.getString(c_name, "");
    }

    public static void setC_name(String c_name) {
        prefs.edit().putString(c_name, c_name).commit();
    }

    public static String c_name = "c_name";


    public void setGender(String genderValue) {
        prefs.edit().putString(gender, genderValue).apply();
    }

    public String getGender() {
        return prefs.getString(gender, genderDefValue);
    }

    private static String isTalent = "isTalent";
    private static boolean isTalentValue;


    public void setTalent(boolean talent) {
        prefs.edit().putBoolean(isTalent, talent).apply();
    }

    public static boolean getTalent() {
        return prefs.getBoolean(isTalent, isTalentValue);
    }

/*  public void setIsLoggedIn(boolean loggedInValue) {
        prefs.edit().putBoolean(loggedIn, loggedInValue).apply();
    }

    public boolean getIsLoggedIn() {
        return prefs.getBoolean(loggedIn, loggedInDefValue);
    }*/
    private static String isModal = "isTalent";
    private static String isModalValue = "";


    public void setIsModal(String modal) {
        prefs.edit().putString(isModal, modal).apply();
    }

    public String getIsModal() {
        return prefs.getString(isModal, isModalValue);
    }


    private static String city = "city";
    private static String cityDefValue = "";


    public void setCity(String cityValue) {
        prefs.edit().putString(city, cityValue).apply();
    }

    public String getCity() {
        return prefs.getString(city, cityDefValue);
    }

    private static String loggedIn = "logged_in";
    private static boolean loggedInDefValue = false;

    public void setIsLoggedIn(boolean loggedInValue) {
        prefs.edit().putBoolean(loggedIn, loggedInValue).apply();
    }

    public boolean getIsLoggedIn() {
        return prefs.getBoolean(loggedIn, loggedInDefValue);
    }


    private static String authToken = "auth_token";
    private static String authTokenValueSt = "";

    public void setAuthToken(String auth) {
        prefs.edit().putString(authToken, auth).apply();
    }

    public String getAuthToken() {
        return prefs.getString(authToken, authTokenValueSt);
    }


    private static String displayName = "auth_token";
    private static String displayNameSt = "";

    public void setDisplayName(String disName) {
        prefs.edit().putString(displayName, disName).apply();
    }

    public String getDisplayName() {
        return prefs.getString(displayName, displayNameSt);
    }

    private static String firebaseToken = "firebase_token";
    private static String firebaseTokenDefValue = "";


    public void setFirebaseToken(String firebaseTokenValue) {
        prefs.edit().putString(firebaseToken, firebaseTokenValue).apply();
    }

    /*public String getFirebaseToken() {
        return prefs.getString(firebaseToken, firebaseTokenDefValue);
    }*/


    private static String accessToken = "accessToken";
    private static String accessTokenValue = "";


    public void setAccessToken(String accessToken) {
        prefs.edit().putString(accessToken, accessToken).apply();
    }

    public String getAccessToken() {
        return prefs.getString(accessToken, accessTokenValue);
    }

    private static String userRole = "user_role";
    private static String userRoleValue = "";


    public void setUserRole(String userRole) {
        prefs.edit().putString(this.userRole, userRole).apply();
    }

    public String getUserRole() {
        return prefs.getString(userRole, userRoleValue);
    }

    public void setUserLoggedIn(Boolean logged) {
        getmPrefs().edit().putBoolean(Constants.SHARED_PREF_NAME, true).apply();
    }

    public Boolean getUserLoggedIn() {
        return getmPrefs().getBoolean(Constants.SHARED_PREF_NAME, false);
    }

    public void clearPreferences() {
        mPrefs.edit().clear().apply();
        mPrefs.edit().clear().commit();
    }

    String timeSt;

    public String localToGMT(String dateSend) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

            Date date = format.parse(dateSend);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date gmt = new Date(sdf.format(date));
            String outputPattern = "yyyy-MM-dd kk:mm:ss";
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            timeSt = outputFormat.format(gmt.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateSend;
    }

    public static Date gmttoLocalDate(Date date) {

        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        return local;
    }
/*

    public void logoutCall() {
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().logoutUser(APIServerResponse.LOGOUT, getAuthToken(), getUserID(), BaseActivity.this);
            } else {
                showSnack(Constants.INTERNET_CONNECTION);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
*/


    public void alertWithSingleTitle(String title, String message, String error) {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(BaseActivity.this);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }



    public static String convertTiming(String checkedIn) {
        String timeConverted = "";
        try {
            Date d = f1.parse(checkedIn);
            timeConverted = f2.format(d).toLowerCase();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return timeConverted;
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null && inputManager != null) {
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                inputManager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

}

package fameuz.com.fameuzapp.ui.navigationdrawer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.SharedPref;

public class Adapter_FragmentMyJobs extends RecyclerView.Adapter<Adapter_FragmentMyJobs.MyViewHolder> {

    private Context activity;
    private SharedPref sharedPref;

    public Adapter_FragmentMyJobs(Context activity) {
        this.activity=activity;
        sharedPref = new SharedPref(activity);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        MyViewHolder(View view) {
            super(view);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_myjobs, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
    }

    @Override
    public int getItemCount() {
        return 10;
    }
}
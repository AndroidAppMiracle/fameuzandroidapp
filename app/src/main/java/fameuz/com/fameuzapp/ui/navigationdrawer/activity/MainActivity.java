package fameuz.com.fameuzapp.ui.navigationdrawer.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.login.activity.LoginActivity;
import fameuz.com.fameuzapp.ui.navigationdrawer.fragment.Fragment_Home;
import fameuz.com.fameuzapp.ui.navigationdrawer.fragment.MyProfileFragment;
import fameuz.com.fameuzapp.ui.navigationdrawer.fragment.ShortListFragment;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.ui.jobs.SearchJobs;
import fameuz.com.fameuzapp.utils.LocaleManager;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    TextView side_username, side_usercat;
    CircleImageView imageView,pendingImg;
    SharedPref sharedPref;
    View navHeader;
    Button btn_shortlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPref = new SharedPref(MainActivity.this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navHeader = navigationView.getHeaderView(0);

        side_username = navHeader.findViewById(R.id.side_username);
        imageView = navHeader.findViewById(R.id.imageView);
        pendingImg = navHeader.findViewById(R.id.pendingImg);
        side_usercat = navHeader.findViewById(R.id.side_usercat);
        sharedPref.setIsonetime("true");
        side_username.setText(sharedPref.getUser_name());
        if (sharedPref.getIs_userage().equalsIgnoreCase("")) {
            side_usercat.setText(sharedPref.getSetC_name());
        } else {
            side_usercat.setText(sharedPref.getSetC_name() + "," + sharedPref.getIs_userage());
        }
        Log.e("hello", "hello " + sharedPref.getUser_img());

        addfragment(new Fragment_Home(), false);

        btn_shortlist = findViewById(R.id.btn_shortlist);

        btn_shortlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPref.getIs_user_verified().equalsIgnoreCase("true"))
                    Common.addfragment(MainActivity.this, new ShortListFragment());
                else
                    Toast.makeText(getApplicationContext(), "" + getString(R.string.user_not_verified), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setImage(){
        if(sharedPref.getUser_img().length()>0) {
            Glide.with(this)
                    .load(sharedPref.getUser_img())
                    .into(imageView);
            pendingImg.setVisibility(sharedPref.getIs_user_verified().equalsIgnoreCase("true") ? View.GONE : View.VISIBLE);
            pendingImg.setImageResource(sharedPref.getLanguageVal().equalsIgnoreCase("en")
                    ? R.drawable.pending_for_approval : sharedPref.getLanguageVal().equalsIgnoreCase("pt")
                    ? R.drawable.pending_for_approval_pt : R.drawable.pending_for_approval_ru);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            addfragment(new Fragment_Home(), false);
        } else if (id == R.id.nav_gallery) {
            addfragment(new MyProfileFragment(), false);
        } else if (id == R.id.nav_slideshow) {
            addfragment(new SearchJobs(), true);
        } else if (id == R.id.nav_manage) {
        } else if (id == R.id.nav_signout) {
            sharedPref.setIsonetime("");
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void addfragment(Fragment fragment, boolean isvalid) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment, fragment.getTag());
        if (isvalid) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isvalid", isvalid);
            fragment.setArguments(bundle);
        }
        fragmentTransaction.commit();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }
}

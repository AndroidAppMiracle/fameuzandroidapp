package fameuz.com.fameuzapp.ui.jobs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.retrofit.model.JobDetailsPojo;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.registration.adapters.Adapter_SearchJob;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.common.SharedPref;
import retrofit2.Response;

public class SearchJobs extends Fragment implements APIServerResponse {

    View view;
    RecyclerView jobs_rv;
    Adapter_SearchJob adapter_searchJob;
    SharedPref sharedPref;
    JSONObject jsonObject;
    Button btn_postjob,reset;
    TextView txt_header;
    EditText search_et;
    ArrayList<JobDetailsPojo> arrayList;
    LinearLayout no_jobll;
    boolean showtoolbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_searchjobs,container,false);
        if(getArguments()!=null){
            showtoolbar = getArguments().getBoolean("isvalid");
        }
        sharedPref = new SharedPref(getActivity());
        ids(view);
        Common.showprogress(getActivity());
        postjobdetail(search_et.getText().toString().trim());
        return view;
    }

    private void ids(View view) {
        jobs_rv = view.findViewById(R.id.jobs_rv);
        btn_postjob = view.findViewById(R.id.btn_postjob);
        txt_header = view.findViewById(R.id.txt_header);
        txt_header.setText(getActivity().getString(R.string.lastest_job));
        search_et = view.findViewById(R.id.search_et);
        reset = view.findViewById(R.id.reset);
        no_jobll = view.findViewById(R.id.no_jobll);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        jobs_rv.setLayoutManager(mLayoutManager);
        jobs_rv.setItemAnimator(new DefaultItemAnimator());

        btn_postjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPref.getIs_user_verified().equalsIgnoreCase("true")) {
                    Common.addfragment(getActivity(), new PostJobs());
                }  else {
                    Toast.makeText(getActivity(), ""+getString(R.string.user_not_verified), Toast.LENGTH_SHORT).show();
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postjobdetail("");
            }
        });

        search_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Common.hideKeyboard(getActivity(),search_et);
                    Common.showprogress(getActivity());
                    postjobdetail(search_et.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });

    }

    public void postjobdetail(String search) {
        ServerAPI.getInstance().getalljobs(APIServerResponse.SEARCHALLJOBS,sharedPref.getLanguageVal(), sharedPref.getApi_key(), search,
                "","","All","All", this);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        Common.dismissprogress();
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        Toast.makeText(getActivity(), ""+getActivity().getString(R.string.internalserver), Toast.LENGTH_SHORT).show();
        Common.dismissprogress();
    }

    @Override
    public void onSuccessString(int tag, String response) {
                Common.dismissprogress();
        Common.dismissprogress();
                switch (tag) {
                    case APIServerResponse.SEARCHALLJOBS:
                        try {
                            jsonObject = new JSONObject(response);
                            Log.e("response","res "+response);
                            if(jsonObject.optBoolean("success")) {
                                arrayList = new ArrayList<>();
                                if(arrayList.size()>0)
                                    arrayList.clear();
                                JSONArray jsonArray = jsonObject.getJSONObject("results").getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JobDetailsPojo jobDetailsPojo = new JobDetailsPojo();
                                    jobDetailsPojo.setIsApproved(jsonArray.getJSONObject(i).optString("is_approved"));
                                    jobDetailsPojo.setJobCategory(jsonArray.getJSONObject(i).optString("job_category"));
                                    jobDetailsPojo.setJobCity(jsonArray.getJSONObject(i).optString("job_city"));
                                    jobDetailsPojo.setJobCompany(jsonArray.getJSONObject(i).optString("job_company"));
                                    jobDetailsPojo.setJobCountry(jsonArray.getJSONObject(i).optString("job_country"));
                                    jobDetailsPojo.setJobCreated(jsonArray.getJSONObject(i).optString("job_created"));
                                    jobDetailsPojo.setJobDescr(jsonArray.getJSONObject(i).optString("job_descr"));
                                    jobDetailsPojo.setJobEdited(jsonArray.getJSONObject(i).optString("job_edited"));
                                    jobDetailsPojo.setJobEnd(jsonArray.getJSONObject(i).optString("job_end"));
                                    jobDetailsPojo.setJobId(jsonArray.getJSONObject(i).optString("job_id"));
                                    jobDetailsPojo.setJobImage(jsonArray.getJSONObject(i).getJSONObject("user").optString("profile_image"));
                                    jobDetailsPojo.setJobTitle(jsonArray.getJSONObject(i).optString("job_title"));
                                    jobDetailsPojo.setJobStart(jsonArray.getJSONObject(i).optString("job_start"));
                                    arrayList.add(jobDetailsPojo);
                                }
                                search_et.setText("");
                                setadapter(arrayList);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }

        }

    private void setadapter(ArrayList<JobDetailsPojo> arrayList) {
        Log.e("arrayList","arrayList "+arrayList.size());

        txt_header.setVisibility(arrayList.size()>0?View.VISIBLE:View.GONE);
        jobs_rv.setVisibility(arrayList.size()>0?View.VISIBLE:View.GONE);
        no_jobll.setVisibility(arrayList.size()>0?View.GONE:View.VISIBLE);

        if(jobs_rv.getVisibility()==View.VISIBLE) {
            adapter_searchJob = new Adapter_SearchJob(getActivity(), arrayList, getActivity().getSupportFragmentManager());
            jobs_rv.setAdapter(adapter_searchJob);
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        if (showtoolbar) {
            Common.toolbar_status(getActivity(), true, "");
        } else {
            Common.toolbar_status(getActivity(), false, "" + getActivity().getString(R.string.lastest_job));
        }
    }

}


package fameuz.com.fameuzapp.ui.navigationdrawer.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import fameuz.com.fameuzapp.Models.CategoryModel;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.common.adapter.CategorySubSpinnerAdapter;
import fameuz.com.fameuzapp.common.Common;
import fameuz.com.fameuzapp.utils.ToastMsg;
import retrofit2.Response;

public class ShortListFragment extends Fragment  implements APIServerResponse{

    View view;
    Spinner categroy_spinner;
    JsonObject jsonObject;
    List<CategoryModel> list = new ArrayList<>();
    CategorySubSpinnerAdapter customAdapter;
    SharedPref sharedPref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view  = inflater.inflate(R.layout.fragment_shortlist,container,false);
        ids(view);
        clicks();
        Common.showprogress(getActivity());
        getAllCategories();

        return view;
    }

    private void clicks() {

        categroy_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void ids(View view) {
        sharedPref=new SharedPref(getActivity());
        categroy_spinner = view.findViewById(R.id.categroy_spinner);
    }

    public void getAllCategories() {
            ServerAPI.getInstance().getAllCategories(APIServerResponse.GET_ALL_CATEGORIES, sharedPref.getLanguageVal(), "0", this);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        Common.dismissprogress();
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.GET_ALL_CATEGORIES:
                        jsonObject = (JsonObject) response.body();
                        if (jsonObject.get("success").getAsBoolean()) {
                            JsonArray jsonArray = jsonObject.get("categories").getAsJsonArray();
                            if (list.size() > 0)
                                list.clear();

                            CategoryModel modelas = new CategoryModel();
                            modelas.setC_name(getActivity().getResources().getString(R.string.select_category));
                            modelas.setC_id("0");
                            list.add(modelas);
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JsonObject obj = jsonArray.get(i).getAsJsonObject();
                                CategoryModel model = new CategoryModel();
                                model.setC_id(obj.get("c_id").getAsString());
                                model.setC_name(obj.get("c_name").getAsString());
                                list.add(model);
                            }

                            customAdapter = new CategorySubSpinnerAdapter(getActivity(), list);

                            categroy_spinner.setAdapter(customAdapter);
                        } else {
                            ToastMsg.showLongToast(getActivity(), jsonObject.get("message").getAsString());
                        }
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onSuccessString(int tag, String response) {

    }
    @Override
    public void onResume() {
        super.onResume();
        Common.toolbar_status(getActivity(),false,""+getActivity().getString(R.string.shortlistedjob));
    }

}

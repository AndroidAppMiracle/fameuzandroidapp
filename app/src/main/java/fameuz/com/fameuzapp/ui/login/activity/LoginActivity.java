package fameuz.com.fameuzapp.ui.login.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.common.GlobalConstant;
import fameuz.com.fameuzapp.common.SharedPref;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.retrofit.ServerAPI;
import fameuz.com.fameuzapp.ui.common.GifImageView;
import fameuz.com.fameuzapp.ui.common.activity.BaseActivity;
import fameuz.com.fameuzapp.ui.login.adapter.SpinnerAdapter;
import fameuz.com.fameuzapp.ui.navigationdrawer.activity.MainActivity;
import fameuz.com.fameuzapp.ui.registration.activity.ActivityVerification;
import fameuz.com.fameuzapp.ui.registration.activity.RegistrationActivity;
import fameuz.com.fameuzapp.ui.registration.activity.SubRegistrationActivity;
import fameuz.com.fameuzapp.utils.LocaleManager;
import fameuz.com.fameuzapp.utils.ToastMsg;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener, APIServerResponse, AdapterView.OnItemSelectedListener {

    final String emailPattern = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    TextInputEditText email_et_id, password_et;
    Button login_btn;
    TextView sign_up_txt;
    Button fb_login_btn;
    Spinner language_spn;
    TextView forget_pwd_txt;
    AlertDialog b;
    JSONObject jsonObject;
    Dialog dialog;
    SharedPref sharedPref;
    String[] langArray = {" English", " Português", " Pусский"};
    Integer[] imageArray = {R.drawable.english, R.drawable.protugese,
            R.drawable.russia};
    CallbackManager callbackManager;
    GifImageView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getHashKey();
        sharedPref = new SharedPref(LoginActivity.this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        init();
    }

    public void changeTextColor(TextView textView) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString beforeblackSpannable = new SpannableString(getResources().getString(R.string.not_a_member));
        beforeblackSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, beforeblackSpannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString afterblackSpannable = new SpannableString(" " + getResources().getString(R.string.here));
        afterblackSpannable.setSpan(new UnderlineSpan(), 0, afterblackSpannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        afterblackSpannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, afterblackSpannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        builder.append(beforeblackSpannable);
        builder.append(afterblackSpannable);

        textView.setText(builder);
    }

    private void getHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "fameuz.com.fameuzapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public void init() {
        email_et_id = findViewById(R.id.email_et_id);
        password_et = findViewById(R.id.password_et);
        login_btn = findViewById(R.id.login_btn);
        fb_login_btn = findViewById(R.id.fb_login_btn);
        sign_up_txt = findViewById(R.id.sign_up_txt);
        forget_pwd_txt = findViewById(R.id.forget_pwd_txt);
        webview = findViewById(R.id.webview);
        language_spn = findViewById(R.id.language_spn);

        changeTextColor(sign_up_txt);
        findViewById(R.id.forget_pwd_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgot_pwd();
            }
        });

        webview.setGifAssetPath(GlobalConstant.stringPath);
        login_btn.setOnClickListener(this);
        fb_login_btn.setOnClickListener(this);
        sign_up_txt.setOnClickListener(this);
        language_spn.setOnItemSelectedListener(this);
        setSpinnerData();
    }

    public void setSpinnerData() {
        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.spinner_sel_lang, langArray, imageArray);
        language_spn.setAdapter(adapter);
        language_spn.setSelection(getLanguageValue().length() > 0 ? adapter.getPosition(getLanguageValue()) : 0);
        setLanguageValue(language_spn.getSelectedItem().toString());
    }

    private boolean validateFields(String username, String password) {
        if (username.length() <= 0) {
            hideSoftKeyboard(LoginActivity.this);
            ToastMsg.showShortToast(LoginActivity.this, getResources().getString(R.string.user_email));
            email_et_id.requestFocus();
            return false;
        } else if (emailPattern.matches(username)) {
            hideSoftKeyboard(LoginActivity.this);
            ToastMsg.showShortToast(LoginActivity.this, getResources().getString(R.string.email_validation));
            email_et_id.requestFocus();
            return false;
        } else if (password.length() <= 0) {
            hideSoftKeyboard(LoginActivity.this);
            ToastMsg.showShortToast(LoginActivity.this, getResources().getString(R.string.password));
            password_et.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                loginCall();
                break;
            case R.id.fb_login_btn:
                loginFacebook();
                break;
            case R.id.sign_up_txt:
                callSignUp();
                break;
            case R.id.forget_pwd_txt:
                openForgetPasswordPopUp();
                break;
            default:
                break;
        }
    }

    public void callSignUp() {
        startActivity(new Intent(this, RegistrationActivity.class));
        finish();
    }

    public void openForgetPasswordPopUp() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custome_layout, null);
        dialogBuilder.setView(dialogView);
        final EditText emailEt = (EditText) dialogView.findViewById(R.id.email_txt_et);
        final Button cancleBtn = (Button) dialogView.findViewById(R.id.cancle_btn);
        Button okBtn = (Button) dialogView.findViewById(R.id.ok_btn);
        cancleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        b = dialogBuilder.create();
        b.show();
    }

    public void loginCall() {
        if (isConnectedToInternet()) {
            if (validateFields(email_et_id.getText().toString(), password_et.getText().toString())) {
                RequestBody requestBody = new FormBody.Builder()
                        .add("email", email_et_id.getText().toString())
                        .add("password", password_et.getText().toString())
                        .build();
                ServerAPI.hitapi(APIServerResponse.LOGIN, requestBody, sharedPref.getLanguageVal(), this);
                showLoading();
            }
        }
    }

    private void loginFacebook() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.e("error", "error " + object.toString());
                                if (response != null) {
                                    String mFbid = object.optString("id");
                                    String memail = object.optString("email");
                                    registerFacebookSignup(mFbid, memail);
                                    if (AccessToken.getCurrentAccessToken() != null) {
                                        LoginManager.getInstance().logOut();
                                    }
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
                Log.e("error", "error " + error.toString());
                Toast.makeText(LoginActivity.this, "" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void registerFacebookSignup(String facebook, String email) {
        if (isConnectedToInternet()) {
            showLoading();
            RequestBody requestBody = new FormBody.Builder()
                    .add("facebook_id", facebook)
                    .add("email", email)
                    .add("client", "android")
                    .build();
            ServerAPI.getInstance().facebookapi(APIServerResponse.SOCIAL_LOGIN, requestBody, sharedPref.getLanguageVal(), this);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
    }

    @Override
    public void onError(int tag, Throwable throwable) {
    }

    @Override
    public void onSuccessString(int tag, String response) {
        hideLoading();
        try {
            jsonObject = new JSONObject(response);
            switch (tag) {
                case APIServerResponse.LOGIN:

                    if (jsonObject.optBoolean("success")) {
                        if (jsonObject.optString("next-step").equalsIgnoreCase("profile-page")) {
                            JSONObject detailUserObjectas = jsonObject.getJSONObject("result").getJSONObject("user");
                            if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                                sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                            } else {
                                sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                            }
                            sharedPref.setUserid(detailUserObjectas.optString("user_id"));
                            sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                            sharedPref.setIsemail(detailUserObjectas.optString("email"));
                            if (jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                                sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                            } else {
                                sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                            }
                            sharedPref.setHasMusicCatgeory(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                            sharedPref.setIsTalent(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                            sharedPref.setIsModel(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                            sharedPref.setIsAgency(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));
                            sharedPref.setIs_user_verified(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                    && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                    ? "true" : "false");
                            sharedPref.setUser_img(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                    && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                    ? detailUserObjectas.optString("profile_image")
                                    : detailUserObjectas.optString("pendingProfileImage"));
                            sharedPref.setIs_userage(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("age"));
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        runOnUiThread(new Thread(new Runnable() {
                            public void run() {
                                try {
                                    if (jsonObject.optString("message").contains("Wrong email or password")) {
                                        Toast.makeText(LoginActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(LoginActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                                        JSONObject detailUserObjectas = jsonObject.getJSONObject("result").getJSONObject("user");
                                        if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                                            sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                                        } else {
                                            sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                                        }
                                        sharedPref.setUserid(detailUserObjectas.optString("user_id"));
                                        sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                                        sharedPref.setUser_img(detailUserObjectas.optString("profile_image"));
                                        sharedPref.setIsemail(detailUserObjectas.optString("email"));
                                        if (jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                                            sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                                        } else {
                                            sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                                        }
                                        sharedPref.setHasMusicCatgeory(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                                        sharedPref.setIsTalent(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                                        sharedPref.setIsModel(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                                        sharedPref.setIsAgency(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));

                                        sharedPref.setIs_userage(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("age"));
                                        if (jsonObject.optString("next-step").equalsIgnoreCase("profile-photo") || jsonObject.optString("next-step").equalsIgnoreCase("new-profile-photo")) {
                                            Intent intent = new Intent(LoginActivity.this, SubRegistrationActivity.class);
                                            GlobalConstant.isuploadphoto = true;
                                            startActivity(intent);
                                            finish();
                                        } else if (jsonObject.optString("next-step").equalsIgnoreCase("activate-account")) {
                                            Intent intent = new Intent(LoginActivity.this, ActivityVerification.class);
                                            startActivity(intent);
                                            finish();
                                        } else if (jsonObject.optString("next-step").equalsIgnoreCase("register-primary-details") || jsonObject.optString("next-step").equalsIgnoreCase("register-model-details")) {
                                            Intent intent = new Intent(LoginActivity.this, SubRegistrationActivity.class);
                                            GlobalConstant.isuploadphoto = false;
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }));
                    }

                    break;
                case SOCIAL_LOGIN:
                    if (jsonObject.optString("message").equalsIgnoreCase("Select Category")) {
                        Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                        intent.putExtra("id", jsonObject.optJSONObject("userDetails").optString("facebook_id"));
                        intent.putExtra("email", jsonObject.optJSONObject("userDetails").optString("email"));
                        startActivity(intent);
                    } else {
                        if (jsonObject.optString("next-step").equalsIgnoreCase("profile-page")) {
                            JSONObject detailUserObjectas = jsonObject.getJSONObject("result").getJSONObject("user");
                            if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                                sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                            } else {
                                sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                            }
                            sharedPref.setUserid(detailUserObjectas.optString("user_id"));
                            sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                            sharedPref.setIsemail(detailUserObjectas.optString("email"));
                            if (jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                                sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                            } else {
                                sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                            }
                            sharedPref.setHasMusicCatgeory(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                            sharedPref.setIsTalent(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                            sharedPref.setIsModel(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                            sharedPref.setIsAgency(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));
                            sharedPref.setIs_userage(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("age"));
                            sharedPref.setIs_user_verified(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                    && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                    ? "true" : "false");
                            sharedPref.setUser_img(jsonObject.getJSONObject("result").getJSONObject("userInfo").getInt("isVerified") == 1
                                    && jsonObject.getJSONObject("result").getJSONObject("userInfo").getBoolean("isApproved")
                                    ? detailUserObjectas.optString("profile_image")
                                    : detailUserObjectas.optString("pendingProfileImage"));
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            runOnUiThread(new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        Toast.makeText(LoginActivity.this, "" + jsonObject.get("message"), 20000).show();
                                        JSONObject detailUserObjectas = jsonObject.getJSONObject("result").getJSONObject("user");
                                        if (!detailUserObjectas.optString("first_name").equalsIgnoreCase("")) {
                                            sharedPref.setUser_name(detailUserObjectas.optString("first_name") + " " + detailUserObjectas.optString("last_name"));
                                        } else {
                                            sharedPref.setUser_name(detailUserObjectas.optString("display_name"));
                                        }
                                        sharedPref.setUserid(detailUserObjectas.optString("user_id"));
                                        sharedPref.setApi_key(detailUserObjectas.optString("api_key"));
                                        sharedPref.setUser_img(detailUserObjectas.optString("profile_image"));
                                        sharedPref.setIsemail(detailUserObjectas.optString("email"));
                                        if (jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories").equalsIgnoreCase("")) {
                                            sharedPref.setSetC_name(detailUserObjectas.optString("c_name"));
                                        } else {
                                            sharedPref.setSetC_name(detailUserObjectas.optString("c_name") + ", " + jsonObject.getJSONObject("result").getJSONObject("userInfo").optString("subCategories"));
                                        }
                                        sharedPref.setHasMusicCatgeory(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("hasMusicCategory"));
                                        sharedPref.setIsTalent(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isTalent"));
                                        sharedPref.setIsModel(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isModel"));
                                        sharedPref.setIsAgency(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("isAgency"));
                                        sharedPref.setIs_userage(jsonObject.getJSONObject("result").getJSONObject("userInfo").getString("age"));
                                        if (jsonObject.optString("next-step").equalsIgnoreCase("profile-photo") || jsonObject.optString("next-step").equalsIgnoreCase("new-profile-photo")) {
                                            Intent intent = new Intent(LoginActivity.this, SubRegistrationActivity.class);
                                            GlobalConstant.isuploadphoto = true;
                                            startActivity(intent);
                                            finish();
                                        } else if (jsonObject.optString("next-step").equalsIgnoreCase("activate-account")) {
                                            Intent intent = new Intent(LoginActivity.this, ActivityVerification.class);
                                            startActivity(intent);
                                            finish();
                                        } else if (jsonObject.optString("next-step").equalsIgnoreCase("register-primary-details") || jsonObject.optString("next-step").equalsIgnoreCase("register-model-details")) {
                                            Intent intent = new Intent(LoginActivity.this, SubRegistrationActivity.class);
                                            GlobalConstant.isuploadphoto = false;
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Toast.makeText(LoginActivity.this, "" + jsonObject.get("message"), Toast.LENGTH_LONG).show();

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }));
                        }
                    }
                    break;

                case APIServerResponse.FORGOT_PWD:
                    if (dialog != null)
                        dialog.dismiss();
                    Toast.makeText(LoginActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("onActivityResult", "requestcode " + requestCode + " result " + resultCode + " data " + data.getAction());
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (!getLanguageValue().equalsIgnoreCase(language_spn.getSelectedItem().toString())) {
            setNewLocale(position == 1 ? LocaleManager.LANGUAGE_PERSIAN
                    : position == 2 ? LocaleManager.LANGUAGE_RUSSIAN
                    : LocaleManager.LANGUAGE_ENGLISH, false);
        }
        setLanguageValue(position == 1 ? " Português" : position == 2 ? " Pусский" : " English");
        sharedPref.setLanguageVal(position == 1 ? "pt" : position == 2 ? "ru" : "en");
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void forgot_pwd() {
        dialog = new Dialog(LoginActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.dialogue_forgot_password);
        EditText edit_frgpwd = dialog.findViewById(R.id.edit_frgpwd);
        Button btn_frg_pwd = dialog.findViewById(R.id.btn_frg_pwd);
        btn_frg_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_frgpwd.getText().toString().matches(emailPattern)) {
                    hitforgotapi(edit_frgpwd.getText().toString());
                } else {
                    Toast.makeText(LoginActivity.this, "" + getString(R.string.email_validation), Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }

    private void hitforgotapi(String email) {
        showLoading();
        ServerAPI.getInstance().forgot_pwd(APIServerResponse.FORGOT_PWD, sharedPref.getLanguageVal(), email, this);
    }

    private boolean setNewLocale(String language, boolean restartProcess) {
        LocaleManager.setNewLocale(this, language);
        if (restartProcess) {
            System.exit(0);
        } else {
            Intent intent = getIntent();
            startActivity(intent);
            finishAffinity();
        }
        return true;
    }
}

package fameuz.com.fameuzapp.ui.navigationdrawer.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import fameuz.com.fameuzapp.R;
import fameuz.com.fameuzapp.retrofit.APIServerResponse;
import fameuz.com.fameuzapp.ui.navigationdrawer.adapter.Adapter_FragmentMyJobs;
import fameuz.com.fameuzapp.common.Common;
import retrofit2.Response;

public class MyJobs extends Fragment implements APIServerResponse{

    View view;
    EditText edit_search_query;
    Spinner spinner_enteries;
    RecyclerView recyclerview_jobs;
    Adapter_FragmentMyJobs adapter_fragmentMyJobs;
    String[] job_adapter = new String[]{"10 entries","25 entries","50 entires","100 entires"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_my_jobs,container,false);
        ids(view);
        return view;
    }

    private void ids(View view) {
        edit_search_query = view.findViewById(R.id.edit_search_query);
        recyclerview_jobs = view.findViewById(R.id.recyclerview_jobs);
        spinner_enteries = view.findViewById(R.id.spinner_enteries);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview_jobs.setLayoutManager(mLayoutManager);
        recyclerview_jobs.setItemAnimator(new DefaultItemAnimator());
        setadapter();
        setspinner_adapter();
    }

    private void setadapter() {
        adapter_fragmentMyJobs = new Adapter_FragmentMyJobs(getActivity());
        recyclerview_jobs.setAdapter(adapter_fragmentMyJobs);
    }

    private void setspinner_adapter() {
        // Creating adapter for spinner
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, job_adapter);
        // Drop down layout style - list view with radio button
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner_enteries.setAdapter(genderAdapter);
    }

    @Override
    public void onSuccess(int tag, Response response) {

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onSuccessString(int tag, String response) {

    }

    @Override
    public void onResume() {
        super.onResume();
        Common.toolbar_status(getActivity(),false,""+getActivity().getString(R.string.myjobs));
    }
}

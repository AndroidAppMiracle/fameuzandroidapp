package fameuz.com.fameuzapp.Interfaces;

import java.util.ArrayList;

/**
 * Created by osvinuser on 15/9/17.
 */

public interface OnItemClickListener {
    void onItemClick(int position);
}
